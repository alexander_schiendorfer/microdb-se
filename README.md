# MicroDB - Database Systems Implementation SS 2012
Linnea Passing, Alexander Schiendorfer, Chris Vaas
---------------------------------------------------------

MicroDB is a mini database system now containing a
buffer manager implementing the 2Q page replacement
strategy for concurrent access.

You can access this repo with SSH or with HTTPS.

Added support for google test - call "make test" to build
buffermanager test.

Refer to makefile for possible targets 

Installing gcc 4.7
http://superuser.com/a/394811
