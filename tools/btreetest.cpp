#include "../segments/SegmentInventory.h"
#include "../buffer/BufferManager.h"
#include "../b_tree/B_Tree.cpp"
#include "../b_tree/RangeIterator.cpp"
#include <iostream>
#include <memory>

using namespace std;

int main() {
    cout << "Hello B⁺-Tree!!" << endl;

    BufferManager bm("./test.db", 500);
    // create si
    SegmentInventory si(bm);
    si.install(); // reset here

    //Create the first segment
    unique_ptr<RegularSegment> rseg = si.createSegment(20,30);

    B_Tree<int>* bt = new B_Tree<int>(bm, *rseg);

    for (int i = 0; i < 20; ++i) {
    	TID newTid;
    	newTid.slotId = 1234;
    	newTid.pageId = (i+1);
    	bt->insert(i+1, newTid);
	}
/*    TID newTid1;
    newTid1.slotId = 1222;
    newTid1.pageId = 1;
    TID newTid4;
    newTid4.slotId = 4555;
    newTid4.pageId = 4;
    TID newTid5;
    newTid5.slotId = 5666;
    newTid5.pageId = 5;
    TID newTid2;
    newTid2.slotId = 2333;
    newTid2.pageId = 2;
    TID newTid3;
    newTid3.slotId = 3444;
    newTid3.pageId = 3;
    TID newTid6;
    newTid6.slotId = 6777;
    newTid6.pageId = 6;
    TID newTid7;
    newTid7.slotId = 7888;
    newTid7.pageId = 7;
    TID newTid8;
    newTid8.slotId = 8999;
    newTid8.pageId = 8;
    TID newTid9;
    newTid9.slotId = 9111;
    newTid9.pageId = 9;
    
    bt->insert(5, newTid5);
    bt->insert(1, newTid1);
    bt->insert(3, newTid3);
    bt->insert(2, newTid2);
    bt->insert(4, newTid4);*/
    bt->remove(1);
    /*bt->insert(6, newTid6);
    bt->insert(1, newTid1);
    bt->insert(7, newTid7);
    bt->insert(8, newTid8);
    bt->insert(9, newTid9);*/
    cout << bt->visualize();
    
    TID* tid;
    tid = bt->lookup(6);
    if (tid != NULL) {
        cout << "PageId:" << tid->pageId << " SlotId:" << tid->slotId << endl;
    }
    
    RangeIterator<int>* rangeIt = bt->lookupRange(4, 7);

    do {
    	TID* tid = rangeIt->get();
    	cout << tid->pageId << "|" << tid->slotId << endl;
    	rangeIt->inc();
    } while(rangeIt->get() != NULL);

    bt->~B_Tree();
}
