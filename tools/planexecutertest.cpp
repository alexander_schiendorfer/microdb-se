#include "../schema/Metadata.h"
#include "../buffer/BufferManager.h"
#include "../segments/SegmentInventory.h"
#include "../operators/PlanExecuter.h"
#include "../operators/PrintOperator.h"
#include <memory>
#include "../segments/RelationSegment.h"
using namespace std;

int main() {
  BufferManager bm("bin/test.db", 30);
  SegmentInventory si(bm);
  si.install();
  unique_ptr<RegularSegment> regSeg = si.createSegment(1,1); // length 1
  
  Metadata metadata(*regSeg);
  metadata.install("tools/student.sql"); // student, lecture, exam

  // add some data
  std::string student = "student";
  unique_ptr<Schema::Relation> rel = std::move(metadata.getRelation(metadata.lookup(student)));
  Schema::Relation* relationPtr = rel.get();
  //unique_ptr<RelationSegment> relSeg = std::move(si.retrieveRelationSegment(metadata.lookup(student), rel.get()));
  unique_ptr<RelationSegment> relSeg = std::move(si.getRelationSegment(metadata.lookup(student), rel.get(), true)); // no initialization of the segment!

  // add a student (1337, Superman)
  std::vector<unique_ptr<Register> > tuple;
  std::unique_ptr<Register> unique1(new Register); unique1->setInteger(1337); unique1->setInt(true);
  std::unique_ptr<Register> unique2(new Register); unique2->setString("Superman"); unique2->setInt(false);
  tuple.push_back(std::move(unique1));
  tuple.push_back(std::move(unique2));
  relSeg->insertTuple(tuple);

  PlanExecuter ex(bm, si, metadata);
  //PrintOperator p = ex.execute("operator/sample.plan");
  PrintOperator p = ex.execute("operators/test.plan");
  p.open();
  while (p.next()) {} // this prints data to std::cout
  p.close();
  
  return 0;
}
