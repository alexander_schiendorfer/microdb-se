#include "../schema/Metadata.h"
#include "../buffer/BufferManager.h"
#include "../segments/SegmentInventory.h"
#include "../segments/RegularSegment.h"
#include "../schema/Schema.h"
#include <string.h>
#include <memory.h>
using namespace std;

int main() {
  BufferManager bm("bin/test.db", 20);
  SegmentInventory si(bm);
  si.install();
  unique_ptr<RegularSegment> regSeg = si.createSegment(1,1); // length 1

  Metadata metadata(*regSeg);
  metadata.install("schema/test.sql"); // creates metadata

  //test lookup: returns segmentId or MAX_VALUE
  std::string s1 = "country";
  std::string s2 = "departmentX";
  std::string s3 = "departmen";
  std::string s4 = "employee";
  std::string s5 = "department";
  std::cout << "Lookup relation \"" << s1 << "\": " << metadata.lookup(s1) << std::endl;
  std::cout << "Lookup relation \"" << s2 << "\": " << metadata.lookup(s2) << std::endl;
  std::cout << "Lookup relation \"" << s3 << "\": " << metadata.lookup(s3) << std::endl;
  std::cout << "Lookup relation \"" << s4 << "\": " << metadata.lookup(s4) << std::endl;
  std::cout << "Lookup relation \"" << s5 << "\": " << metadata.lookup(s5) << std::endl;



  // Answer all questions for relation "employee"
  std::cout << std::endl << std::endl << "===   My Schema Questionnaire :-)   ===" << std::endl;

  // Which segment holds relation r?
  std::string emp = "employee";
  unsigned int segmentId = metadata.lookup(emp);
  std::cout << "Which segment holds relation \"" << emp << "\"?" << std::endl;
  std::cout << "--> Segment with ID " << segmentId << std::endl;
  
  
  unique_ptr<Relation> rel = metadata.getRelation(segmentId);

  // Which indexes do I have on relation r?
  unsigned int indexSegmentId = rel->primaryKeySegmentId;
  vector<unsigned> primaryKey = rel->primaryKey;
  std::cout << "Q: Which indexes do I have on relation \"" << emp << "\"?" << std::endl;
  std::cout << "A: PrimaryKey index is on segment " << indexSegmentId << " with columns ";
  for (auto k : primaryKey) {
    std::cout << k << ", ";
  }
  std::cout << std::endl;
  
  // What is the type of attribute a?
  for (unsigned int i = 0; i < rel->attributes.size(); i++) {
    Types::Tag type =  rel->attributes[i].type;
    std::cout << "Q: What is the type of attribute \"" << rel->attributes[i].name << "\"?" << std::endl;
    
    if (type == Types::Tag::Integer) {
      std::cout << "A: Integer" << std::endl; 
    } else {
      std::cout << "A: Char(" << rel->attributes[i].len << ")" << std::endl;
    }
  }
  
  // Given TID t, where do I find attribute a?
  for (unsigned int i = 0; i < rel->attributes.size(); i++) {
    std::cout << "Q: Given TID t, where do I find attribute \"" << rel->attributes[i].name << "\"?" << std::endl;
    unsigned int offset = 0;
    for (unsigned int k = 0; k < i; k++) {
      if (rel->attributes[k].type == Types::Tag::Integer) {
        offset += sizeof(int);
      } else { // fixed-length char
        offset += rel->attributes[k].len;
      }
    }
    std::cout << "A: Offset from TID-Start is " << offset << " bytes" << std::endl;
  }

  return 0;
}
