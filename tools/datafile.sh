#!/bin/bash

if [[ $# = 0 || $1 = "-h" ]]; then
    echo "creates datafile for microdb" 
    echo "usage: ./datafile.sh <filename> <buffercount>"
    echo "for example: ./datafile.sh mydb.db 20"
    exit 0;
fi

dd if=/dev/zero of=$1 count=$2 bs=4096
