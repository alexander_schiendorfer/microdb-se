#include <vector>
#include <string>
#include "../operators/Operator.h"
#include "../operators/MergeOperator.h"
#include "../operators/PrintOperator.h"

using namespace std;

class MockingOperator : public Operator {
	struct privRec {
		std::string first;
		int second;
		std::string third;
	};

	privRec* records;
	int curRec;

public:
	MockingOperator(bool sw) {
		records = new privRec[3];

		output.resize(3, NULL);
		for(int i = 0; i < 3; i++)
			output[i] = new Register();

		output[0]->setInt(false);
		output[1]->setInt(true);
		output[2]->setInt(false);

		if (sw) {
			records[0].first = "Oliver Kahn";
			records[0].second = 1;
			records[0].third = "7";

			records[1].first = "Jens Lehmann";
			records[1].second = 1;
			records[1].third = "0";

			records[2].first = "Thomas Linke";
			records[2].second = 2;
			records[2].third = "7";
		} else {
			records[0].first = "Torwart";
			records[0].second = 1;
			records[0].third = "Passt auf das Tor auf";

			records[1].first = "Abwehr";
			records[1].second = 2;
			records[1].third = "Verteidigt das Tor";

			records[2].first = "Trainer";
			records[2].second = 3;
			records[2].third = "Haelt das Team zusammen";
		}
	}

	virtual ~MockingOperator() {
		for(int i = 0; i < 3; i++) {
			delete output[i];
		}
		delete[] records;
	}

	void open() {
		curRec = 0;

	}
	// Produce the next tuple
	bool next() {
		if(curRec >= 3)
			return false;
		else {
			output[0]->setString(records[curRec].first);
			output[1]->setInteger(records[curRec].second);
			output[2]->setString(records[curRec].third);

			curRec++;
			return true;
		}
	}
	// Get all produced values
	std::vector<Register*> getOutput() {
		return output;
	}

	// Close the operator
	void close() {

	}
};

int main(int argc, char **argv) {
	MockingOperator mock1(true);
	PrintOperator po1(&mock1, cout);
	po1.open();

	while(po1.next());

	po1.close();

	cout << "-----------------" << endl;

	MockingOperator mock2(false);
	PrintOperator po2(&mock2, cout);
	po2.open();

	while(po2.next());

	po2.close();

	cout << "-----------------" << endl;

	pair<unsigned int, unsigned int> att (1,1);
	vector<pair<unsigned int, unsigned int>> atts;
	atts.push_back(att);
	MergeOperator mo(&mock1, &mock2, atts);
	PrintOperator po(&mo, cout);
	po.open();

	while(po.next());

	po.close();
}
