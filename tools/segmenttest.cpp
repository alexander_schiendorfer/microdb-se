#include "../segments/SPSegment.h"
#include "../segments/SegmentInventory.h"
#include "../buffer/BufferManager.h"
#include <iostream>
#include <memory>
using namespace std;

int main() {

    BufferManager bm("bin/test.db", 20);
    // create si
    SegmentInventory si(bm);
    si.install(); // reset here
    
    // do some tests
    si.createSegment(1,3); // first segment gets 3 pages
    si.createSegment(1,2); // second segment gets 2 pages
    si.growSegment(0); // first segment gets 1.25*3 = 3 pages
    si.createSegment(1,5); // third segment gets the last page

    unique_ptr<RegularSegment> r = si.retrieveSegment(0);
    cout << "size of first segment: " << r->getSize() << endl;

    si.dropSegment(1);
    si.dropSegment(2);
    si.dropSegment(0);

    // SegmentInventory cleaned

    si.createSegment(1,2); // id=0, pages 1,2
    si.createSegment(1,1); // id=1, pages 3
    si.createSegment(1,2); // id=2, pages 4,5
    si.createSegment(1,1); // id=3, pages 6
    si.createSegment(1,2); // id=4, pages 7,8
    si.createSegment(1,1); // id=5, pages 9

    cout << "next test" << endl;

    si.dropSegment(0);
    si.dropSegment(4);
    si.dropSegment(5); // pred found
    si.dropSegment(2); // none found, insert in middle of freeList

    // Hexdump-Tool:
    // user@computer$ hd bin/test.db
    /*
      00000000  02 00 00 00 07 00 00 00  03 00 00 00 01 00 00 00  |................|
      00000010  03 00 00 00 01 00 00 00  03 00 00 00 06 00 00 00  |................|
      00000020  01 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
      00000030  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
      *
      00000fe0  00 00 00 00 00 00 00 00  01 00 00 00 02 00 00 00  |................|
      00000ff0  04 00 00 00 02 00 00 00  07 00 00 00 03 00 00 00  |................|
      00001000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
      *
      00032000

	(first row: extentCount=2, freeSpace=7, freeSpaceCount=3, segID of first extent=1)
        (second row: startPage of first extent=3, length of first extent=1, segID second extent=3, startPage of second extent=6)
        (...)
        (row fe0 and ff0 contain 3 freeListEntries)
    */
}
