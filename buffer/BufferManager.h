#ifndef DBS_BUFFER_MAN_H
#define DBS_BUFFER_MAN_H

#include "BufferFrame.h"
#include <string>
#include <deque>
#include <map>
#include <memory>     // for unique_ptr

using namespace std;
#define PAGESIZE 4096
/**
 * BufferManager performs paging for higher levels using the
 * 2Q strategy (http://www.vldb.org/conf/1994/P439.PDF)
 *
 * TODO add paragraph about how 2Q is implemented here
 */
class BufferManager {

  private:
    char* buffer;
    unsigned currentSize;        // needed to check if buffer is already full then only replacements
    unsigned size;               // actual number of buffer frames available
    unsigned Kin, Kout;          // size recommendation constants
    const string& filename;      // used file, stored for possible debug reasons
    int dbFileDescr;             // file descriptor of db-file
    pthread_rwlock_t globalLock; // blocks hashMap and all other data structures

    // datastructures involved
    map<unsigned, unique_ptr<BufferFrame> > hashMap;
    deque<BufferFrame*> A1in;  // for most recently first accessed FIFO organized
    deque<BufferFrame*> Am;    // used more than once -> LRU organized
    deque<unsigned> A1out;     // paged out -> on their way into Am

    // private helper methods
    void pageIn(BufferFrame& toPageIn); // buffer frame knows all pointers to perform a read
    void pageOut(BufferFrame& toPageOut);
    unique_ptr<BufferFrame> reclaimFor(unsigned pageId);
    unique_ptr<BufferFrame> getNewFrame(unsigned pageNo, void* data );

  public:
    /**
     * load filename for reading and preallocate data structures
     */
	BufferManager(const std::string& filename, unsigned size);
	
	/**
	 * write all dirty frames to disk
	 */
	~BufferManager();

	/**
	 * Returns the demanded page if possible
	 * -> if there are locks conflicting with the exclusive setting
	 * method waits
	 * @throw runtime_error if no free space is available
	 */
    BufferFrame& fixPage(unsigned pageId, bool exclusive);

    /**
     * Releases page represented by frame indicating whether it
     * was changed or not (isDirty) thus makes it available
     * for paging out
     */
    void unfixPage(BufferFrame& frame, bool isDirty);

    /**
     * Returns whether this page is currently in main memory
     * used for testing purposes and to avoid friending or other
     * options to make private variables accessible (e.g. not including gtest
     * in production code )
     */
    bool inMainMemory(unsigned pageId);

    /**
     * Returns size
     * needed by SegmentInventory
     */
    unsigned int getSize();
}; 

 
#endif
