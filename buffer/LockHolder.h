// LockHolder

#ifndef DBS_LOCKHOLDER_H
#define DBS_LOCKHOLDER_H

#include <pthread.h>
using namespace std;
/**
Implementation in one class since - easier
*/
class LockHolder {
  private:
    pthread_mutex_t* mutex;
    
  public:
  LockHolder(pthread_mutex_t* mutex) {
  	pthread_mutex_lock(mutex);
  	this->mutex = mutex;
  } 
  
  ~LockHolder() {
    pthread_mutex_unlock(mutex);
  }
};
#endif
