// Implementation for BufferManager.h

#include "BufferManager.h"
#include "LockHolder.h"
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>   // for open/close ...
#include <memory>     // for unique_ptr
#include <stdexcept>  // for runtime_error

// you don't have to write std at every position then
using namespace std;

BufferManager::BufferManager(const string& filename, unsigned size):
		size(size),filename(filename) {
  unsigned bufferSize = size * PAGESIZE; 
  buffer = new char[bufferSize];
  Kin =  size / 4;
  Kout = size / 2;
  currentSize = 0;
  dbFileDescr = open(filename.c_str(), O_RDWR);
  if(dbFileDescr < 0) {
	  std::cerr << "Error occurred when opening " << filename << ": " << errno << std::endl;
	  throw runtime_error("Could not open file " + filename);
  }
  pthread_rwlock_init(&globalLock, NULL);
}
	
	// write all dirty frames to disk
BufferManager::~BufferManager() {
  map<unsigned,unique_ptr<BufferFrame> >::iterator mapIt = hashMap.begin();
  while(mapIt != hashMap.end()) {
	  BufferFrame* frame = mapIt->second.get();
	  if(frame->tryLock(true)) {
		  pageOut(*frame);
		  frame->releaseLock();
	  }
	  mapIt++;
  }
  close(dbFileDescr);
  pthread_rwlock_destroy(&globalLock);
  delete[] buffer;
}

unique_ptr<BufferFrame> BufferManager::reclaimFor(unsigned pageId) {
	BufferFrame* toPageOut = NULL;
	BufferFrame* pageOutCandidate = NULL;

	if(currentSize < size) { // still some space available
		return getNewFrame(pageId, buffer + (currentSize++) * PAGESIZE);
	} else {
		if (A1in.size() > Kin) {
	    deque<BufferFrame*>::reverse_iterator rit = A1in.rbegin();
	    //Search the next !fixed frame

	    while(rit < A1in.rend()) {
	      pageOutCandidate = *rit;
	      if (pageOutCandidate->tryLock(true)) { // we found our target!
	        toPageOut = pageOutCandidate;
	    	pageOut(*toPageOut);
	        A1in.erase(rit.base() - 1);  // offset due to reverse iterator
	        A1out.push_front(toPageOut->getPageNo());
	        if(A1out.size() > Kout) {
	          A1out.pop_back();
	        }
	        break;
	      } // isFixed
	      ++rit;
	    } // while
	    //Should not be reached otherwise exception
	  } // |A1in| > Kin

	  if(toPageOut == NULL) { // either |A1in| < Kin or no deleteFrame available in A1in
	    deque<BufferFrame*>::reverse_iterator rit = Am.rbegin();
	    //Search the next !fixed frame

	    while(rit < Am.rend()) {
	      pageOutCandidate = *rit;
	      if (pageOutCandidate->tryLock(true)) {
	         toPageOut = pageOutCandidate;
	         Am.erase(rit.base() - 1);  // offset due to implementation of reverse iterator
	    	 pageOut(*toPageOut);
	         break;

	      }
	      ++rit;
	     }
	  }

	    if(toPageOut != NULL) {
	      toPageOut->broadcast();
	      unique_ptr<BufferFrame> newFrame =  getNewFrame(pageId, toPageOut->getData());

	      hashMap.erase(toPageOut->getPageNo());
	      return newFrame;
	    } else {
	    	throw runtime_error("Buffer full and no page can be paged out");
	    }
	  }
}

unique_ptr<BufferFrame> BufferManager::getNewFrame(unsigned pageNo, void* data ) {
    unique_ptr<BufferFrame> newFrame(new BufferFrame(data, pageNo));
	newFrame->tryLock(true); // lock exclusively, actually nothing can happen here since no pointer to that object available
    newFrame->setPageNo(pageNo);
    pageIn(*newFrame);
    return newFrame;
}

void BufferManager::pageIn(BufferFrame& toPageIn) {
	pread(dbFileDescr, toPageIn.getData(), PAGESIZE, toPageIn.getPageNo() * PAGESIZE);
	//std::cout << "paging in ... " << toPageIn.getPageNo() << std::endl;
}

void BufferManager::pageOut(BufferFrame& toPageOut) {
	if(toPageOut.isDirty())
	  pwrite(dbFileDescr, toPageOut.getData(), PAGESIZE, toPageOut.getPageNo() * PAGESIZE);
	//std::cout << "paging out ... " << toPageOut.getPageNo() << std::endl;
}

// accessing a page pageId
BufferFrame& BufferManager::fixPage(unsigned int pageId, bool exclusive) {
  // iterator auf queue element
    BufferFrame* frame;
    deque<BufferFrame*>::iterator dequeIt;
    map<unsigned,unique_ptr<BufferFrame> >::iterator mapIt;
    deque<unsigned>::iterator aoutIt;

    while(true) {
      // acquire global write lock
      pthread_rwlock_wrlock(&globalLock);

      // look up pageId in hashMap
      if((mapIt = hashMap.find(pageId)) != hashMap.end()) { // element in map
    	  if((dequeIt = find(A1in.begin(), A1in.end(), mapIt->second.get())) != A1in.end()) {
    		 frame = (*dequeIt);
    		 pthread_mutex_lock(frame->getCondMutex());
    		 if(!frame->tryLock(exclusive)) {
    			 frame->addWaiter();
    			 pthread_rwlock_unlock(&globalLock);
    			 frame->waitForCond();
    			 frame->unlockCond();
    			 continue;
    		 }
    		 pthread_mutex_unlock(frame->getCondMutex());
    	  } // in A1in
    	  else { // must be in Am then
    		  dequeIt = find(Am.begin(), Am.end(), mapIt->second.get());
    		  frame = (*dequeIt);
    		  pthread_mutex_lock(frame->getCondMutex());
    		  if(!frame->tryLock(exclusive)) {
    			frame->addWaiter();
    		  	pthread_rwlock_unlock(&globalLock);
    		  	frame->waitForCond();
    		  	frame->unlockCond();
    		  	continue;
    		  }
    		  pthread_mutex_unlock(frame->getCondMutex());
    		  // implement LRU semantic -> remove from queue and insert in front
    		  Am.erase(dequeIt);
    		  Am.push_front(frame);
    	  } // in Am

    	  frame->changeLock(exclusive);
    	  pthread_rwlock_unlock(&globalLock);
    	  return *frame;
      } // in main memory
      else {
    	  if((aoutIt = find(A1out.begin(), A1out.end(), pageId)) != A1out.end()) {
    		  //std::cout << "But I found it in A1out " << std::endl;
    		  unique_ptr<BufferFrame> newFrame = reclaimFor(pageId);
    		  frame = newFrame.get();
    		  Am.push_front(newFrame.get());
    		  hashMap[pageId].reset(newFrame.release());
    	  } else { // not in aout
    		  unique_ptr<BufferFrame> newFrame = reclaimFor(pageId);
    		  frame = newFrame.get();
    	      A1in.push_front(newFrame.get());
    	      hashMap[pageId].reset(newFrame.release());
    	  }
    	  frame->changeLock(exclusive);
    	  pthread_rwlock_unlock(&globalLock);
    	  return *frame;
      } // not in main memory
    } // while true
}

void BufferManager::unfixPage(BufferFrame& frame, bool isDirty) {
  // acquire global lock
  pthread_rwlock_wrlock(&globalLock);
  if (isDirty) frame.setDirty();
  frame.releaseLock();
  pthread_rwlock_unlock(&globalLock);
}

bool BufferManager::inMainMemory(unsigned pageId) {
	pthread_rwlock_wrlock(&globalLock);
	bool inMem = hashMap.find(pageId) != hashMap.end();
	pthread_rwlock_unlock(&globalLock);
	return inMem;
}

unsigned int BufferManager::getSize() {
    return size;
}
