// Implementation for BufferManager.h

#include "BufferFrame.h"
#include "LockHolder.h"

#include <pthread.h>
#include <iostream>
// you don't have to write std at every position then
using namespace std;

BufferFrame::BufferFrame(void* data, unsigned pageNo) {
  // no lock since constructor will make latch available after return of reference
  this->data = data;
  this->dirty = false;
  this->LSN = 0;
  this->pageNo = pageNo;
  this->waiters = 0;

  pthread_rwlock_init(&frameLock, NULL);
  pthread_cond_init(&cond, NULL);
  pthread_mutex_init(&condMutex, NULL);
  pthread_mutex_init(&waitersMutex, NULL);
}

BufferFrame::~BufferFrame() {
  pthread_rwlock_unlock(&frameLock);
  pthread_cond_destroy(&cond);
  pthread_mutex_destroy(&condMutex);
  pthread_mutex_destroy(&waitersMutex);
  pthread_rwlock_destroy(&frameLock);
}
    
void* BufferFrame::getData() const{
   return this->data;   
}

bool BufferFrame::isDirty() const {
   return this->dirty;   
}

unsigned BufferFrame::getLSN() const {
   return this->LSN;   
}

unsigned BufferFrame::getPageNo() const{
   return this->pageNo;   
}
    
void BufferFrame::setDirty()  {
   this->dirty = true;   
}

void BufferFrame::setPageNo(unsigned pageNo)  {
   this->pageNo = pageNo;   
}

pthread_rwlock_t* BufferFrame::getLock() {
  return &frameLock;
}

pthread_cond_t* BufferFrame::getCond() {
  return &cond;
}

void BufferFrame::waitForCond() {
	pthread_cond_wait(&cond, &condMutex);
}

bool BufferFrame::tryLock(bool exclusive) {
	if(exclusive) {
		return pthread_rwlock_trywrlock(&frameLock) == 0;
	} else {
		return pthread_rwlock_tryrdlock(&frameLock) == 0;
	}
}

void BufferFrame::releaseLock() {
	  pthread_mutex_lock(&condMutex);
	  pthread_rwlock_unlock(&frameLock); // this condition has to become true!
	  broadcast();
	  pthread_mutex_unlock(&condMutex);

}

void BufferFrame::broadcast() {
	LockHolder lh(&waitersMutex);
	  if(waiters > 0) {
	    pthread_cond_broadcast(&cond);
	  }
}

void BufferFrame::changeLock(bool exclusive) {
  pthread_rwlock_unlock(&frameLock);
  tryLock(exclusive);
}

void BufferFrame::unlockCond(){
	LockHolder lh(&waitersMutex);
	waiters--;
	pthread_mutex_unlock(&condMutex);
}

void BufferFrame::addWaiter() {
	LockHolder lh(&waitersMutex);
	waiters++;
}

unsigned BufferFrame::getWaiters() {
	LockHolder lh(&waitersMutex);
	return waiters;
}

pthread_mutex_t* BufferFrame::getCondMutex() {
	return &condMutex;
}

