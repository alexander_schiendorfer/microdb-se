/*
 * BufferFrameHolder.cpp
 *
 *  Created on: May 14, 2012
 *      Author: alexander
 */

#include "BufferFrameHolder.h"

BufferFrameHolder::BufferFrameHolder(BufferManager& bm, unsigned pageId, bool exclusive) : bufferManager(bm), pageId(pageId), frame(bm.fixPage(pageId, exclusive)) {
	isDirty = true;
	lockedPages = NULL;
}

BufferFrameHolder::BufferFrameHolder(BufferManager& bm, unsigned pageId, bool exclusive, std::list<unsigned>* lockedPages) : bufferManager(bm), pageId(pageId), frame(bm.fixPage(pageId, exclusive)), lockedPages(lockedPages) {
	isDirty = true;
	lockedPages->push_back(pageId);
}

BufferFrameHolder::~BufferFrameHolder() {
	bufferManager.unfixPage(frame, isDirty);
	if(lockedPages != NULL)
		(*lockedPages).remove(pageId);
}

void BufferFrameHolder::setDirty(bool dirty) {
	this->isDirty = dirty;
}

BufferFrame& BufferFrameHolder::getProtectedFrame() {
	return frame;
}
