/*
 * BufferFrameHolder.h
 *
 * RAII pattern for buffer frames
 * use to fix and unfix page in an
 * exception safe manner
 *
 *  Created on: May 14, 2012
 *      Author: Alexander Schiendorfer
 */

#ifndef BUFFERFRAMEHOLDER_H_
#define BUFFERFRAMEHOLDER_H_

#include "BufferManager.h"
#include "BufferFrame.h"
#include <list>

class BufferFrameHolder {
private:
	BufferManager& bufferManager;
	unsigned pageId;
	BufferFrame& frame;
	bool isDirty;
	std::list<unsigned>* lockedPages;

public:
	BufferFrameHolder(BufferManager& bm, unsigned pageId, bool exclusive);
	BufferFrameHolder(BufferManager& bm, unsigned pageId, bool exclusive, std::list<unsigned>* lockPageList);
	virtual ~BufferFrameHolder();

	// access method for clients
	BufferFrame& getProtectedFrame();

	// default true, but can be set to false if no write is required
	void setDirty(bool dirty);
};

#endif /* BUFFERFRAMEHOLDER_H_ */
