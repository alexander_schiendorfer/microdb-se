#ifndef DBS_BUFFER_FRAME_H
#define DBS_BUFFER_FRAME_H

#include <pthread.h>

class BufferFrame {
  private :
    unsigned pageNo;  // offset of page on disk
    void* data;       // pointer to buffer, can be used to calculate offset

    pthread_cond_t cond;
    // extra mutex required for condition; cannot use rwlock
    pthread_mutex_t condMutex;
    pthread_mutex_t waitersMutex;

    pthread_rwlock_t frameLock;
    unsigned waiters; // number of waiting threads / protected by condMutex
    unsigned LSN;
    bool dirty;
  public: 
 
    // BufferManager has to make sure, data is loaded from file[pageNo]
    BufferFrame(void* data, unsigned pageNo);
    ~BufferFrame();
    
    void* getData() const;
    bool isDirty() const;
    unsigned getLSN() const;
    unsigned getPageNo() const;

    void setDirty();
    void setPageNo(unsigned pageNo) ;
    pthread_rwlock_t* getLock();
    pthread_cond_t* getCond();
    pthread_mutex_t* getCondMutex();
    bool tryLock(bool exclusive);
    bool tryCondLock();
    void addWaiter();
    unsigned getWaiters();
    void waitForCond();
    void releaseLock();

    /**
     * during creation of bufferFrame it is locked exclusively
     * call this method to enable read/write lock
     */
    void changeLock(bool exclusive);
    void broadcast();
    void unlockCond();
}; 

 
#endif
