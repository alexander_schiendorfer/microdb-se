CXXFLAGS = -std=c++11 -ggdb -Wall -fmessage-length=0

include buffer/LocalMakefile
include segments/LocalMakefile
include tools/LocalMakefile
include util/LocalMakefile
include b_tree/LocalMakefile
include operators/LocalMakefile
include schema/LocalMakefile

# Create directories if needed
CHECKDIR=@mkdir -p $(dir $@)
CXX=g++-4.7
BIN_DIR = bin

# Compile command
define cc-command
	$(CHECKDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@ -lpthread
endef

$(BIN_DIR)/%.o : tools/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : buffer/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : segments/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : util/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : b_tree/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : operators/%.cpp
	$(cc-command)
$(BIN_DIR)/%.o : schema/%.cpp
	$(cc-command)
	
all: buffertest segmenttest btree schematest operators exec

clean: clean-test
	rm -Rf $(BIN_DIR)

exec: $(addprefix $(BIN_DIR)/,$(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_schema:.cpp=.o) $(src_operators:.cpp=.o) $(src_tools_exec:.cpp=.o))
	$(CHECKDIR)
	$(CXX) -o bin/$@ $(CXXFLAGS) $(addprefix $(BIN_DIR)/, $(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_schema:.cpp=.o) $(src_operators:.cpp=.o) $(src_tools_exec:.cpp=.o)) -lpthread

btree: $(addprefix $(BIN_DIR)/,$(src_b_tree:.cpp=.o) $(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_tools_btree:.cpp=.o) $(src_operators:.cpp=.o) $(src_schema:.cpp=.o))
	$(CHECKDIR)
	$(CXX) -o bin/$@ $(CXXFLAGS) $(addprefix  $(BIN_DIR)/, $(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_b_tree:.cpp=.o) $(src_tools_btree:.cpp=.o) $(src_operators:.cpp=.o) $(src_schema:.cpp=.o)) -lpthread

buffertest: $(addprefix $(BIN_DIR)/,$(src_buffer:.cpp=.o) $(src_tools:.cpp=.o))
	$(CHECKDIR)
	$(CXX) -o bin/$@ $(CXXFLAGS) $(addprefix  $(BIN_DIR)/, $(src_buffer:.cpp=.o) $(src_tools:.cpp=.o)) -lpthread

segmenttest: $(addprefix $(BIN_DIR)/,$(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_tools_seg:.cpp=.o) $(src_operators:.cpp=.o) $(src_schema:.cpp=.o))
	$(CHECKDIR)
	$(CXX) -o bin/$@ $(CXXFLAGS) $(addprefix  $(BIN_DIR)/, $(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_tools_seg:.cpp=.o) $(src_operators:.cpp=.o) $(src_schema:.cpp=.o)) -lpthread

schematest: $(addprefix $(BIN_DIR)/,$(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_schema:.cpp=.o) $(src_tools_schema:.cpp=.o) $(src_operators:.cpp=.o))
	$(CHECKDIR)
	$(CXX) -o bin/$@ $(CXXFLAGS) $(addprefix  $(BIN_DIR)/, $(src_buffer:.cpp=.o) $(src_segments:.cpp=.o) $(src_schema:.cpp=.o) $(src_tools_schema:.cpp=.o) $(src_operators:.cpp=.o)) -lpthread

include gtest-make
