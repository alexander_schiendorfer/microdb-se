#ifndef H_TYPES_H
#define H_TYPES_H

#include <string>
#include <cmath>
#include <cstdint>
#include <string.h>

/**
 * Types
 */
namespace Types {
   enum class Tag : unsigned {Integer, Char};
}


/**
 * Integer
 */
typedef int Integer;


/**
 * Char
 */
template <unsigned len>
struct Char {
   char data[len];
   void loadString(const std::string& str);
   std::string toString();
};

template <unsigned len>
void Char<len>::loadString(const std::string& str) {
   if (str.size() >= len) {
      memcpy(data, str.c_str(), len);
   } else {
      memset(data, ' ', len);
      memcpy(data, str.c_str(), str.size());
   }
}

template <unsigned len>
std::string Char<len>::toString() {
   return std::string(data, data+len);
}
#endif
