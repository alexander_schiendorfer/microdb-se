#ifndef METADATA_H
#define METADATA_H
#include "Schema.h"
#include "Types.h"
#include "../segments/RegularSegment.h"
#include "../buffer/BufferManager.h"
#include "../segments/Segment.h"
#include "../segments/Structs.h"
#include <string.h>
#include <memory.h>

namespace MetadataStructs {
struct Header {
  unsigned int relCount; // number of relations (= length of list)
  unsigned int heapStart; // offset to the begin of the heap
};

struct Relation {
  unsigned int segmentId;
  unsigned int nameStart; // offset to the begin of the name
  unsigned int nameLength; // length of name in sizeof(char)
  unsigned int pkId; // Id of the RegularSegment that contains the B-Tree-Index
  unsigned int pkStart; // offset to the begin of the PK list
  unsigned int pkCount; // number of elements in PK list (each element is an unsigned int containing Attribute.id)
  unsigned int attrStart; // offset to the begin of the attribute list
  unsigned int attrCount; // number of elements in attribute list
};

struct Attribute {
  unsigned int id; // position of attribute in relation (0 = leftmost attribute)
  Types::Tag type; // integer or char? (in hexdump, char = "01 00 00 00")
  unsigned int charLength; // if char: length 
  bool notNull; // (in hexdump, true = "01 00 00 00")
  unsigned int nameStart; // offset to the begin of the name
  unsigned int nameLength; // length of name in sizeof(char)
};
}

/*
 * Extension to provide answers to the questions
 */
struct Relation : public Schema::Relation {
  unsigned primaryKeySegmentId;
  Relation(const std::string& name) : Schema::Relation(name) {}
  unsigned int getIndexId() {return primaryKeySegmentId;}
};

/*
 * Uses only one page at the moment.
 *
 * Page layout:
 * Page starts with Header (8 bytes). Next ist the list of relations, each is 28 bytes long. All variable-length parts are stored in the "heap" that starts at the end of the page and is referred to in "nameStart" etc. Heap contains names of relations and attributes, primary key lists and attribute lists (each attribute is 24 bytes long).
 */
class Metadata : public RegularSegment { 
  private: 
  
  public:
  /**
   *
   */
  Metadata(RegularSegment& seg);

  /**
   * Reads schema file, parses it and stores the metadata.
   */
  void install(const std::string& filename);

  /**
   * Returns relationId. If no RelationName matches, -1 is returned.
   */
  unsigned int lookup(string& relationName);

  /**
   * Creates schema object for easy metadata access.
   */
  unique_ptr<Relation> getRelation(unsigned int relationId);

};



#endif
