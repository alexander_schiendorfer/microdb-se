#include "Metadata.h"
#include "Schema.h"
#include "Parser.h"
#include "../segments/RegularSegment.h"
#include "../buffer/BufferManager.h"
#include "../segments/Segment.h"
#include "../segments/SegmentInventory.h"
#include "../segments/Structs.h"
#include <string.h>
#include <iostream>
#include <memory.h>

Metadata::Metadata(RegularSegment& seg) : RegularSegment(seg) {
  // nothing
}

void Metadata::install(const std::string& filename) {
  // parse file
  Parser p(filename);
  std::unique_ptr<Schema> schema;
  try {
    schema = p.parse();
  } catch(ParserError e) {
    std::cout << "Error: " << e.what() << std::endl;
  }

  // print parsed schema
  //std::cout << (*schema).toString() << std::endl;

  // get page
  unsigned int pageNo = list.front().startPage;
  BufferFrame& page = bufferManager.fixPage(pageNo, true); // lock exclusively
  
  // set header
  MetadataStructs::Header * headerpointer = static_cast<MetadataStructs::Header *>(page.getData());

  headerpointer->relCount = 0; // will be incremented when a relation is added to metadata
  headerpointer->heapStart = PAGESIZE; // no heap yet, therefore end of page

  // for each relation: count it, create it, add it to metadata
  for (const Schema::Relation& rel : (*schema).relations) {;
    MetadataStructs::Relation * relpointer = reinterpret_cast<MetadataStructs::Relation *>(static_cast<char *>(page.getData()) + sizeof(MetadataStructs::Header) + headerpointer->relCount*sizeof(MetadataStructs::Relation));
    headerpointer->relCount = headerpointer->relCount + 1;

    // create RelationSegment and store its Id
    unsigned id = segmentInventory.createSegmentGiveId(1,1);
    unique_ptr<RelationSegment> rs = segmentInventory.retrieveRelationSegment(id, const_cast<Schema::Relation*>(&rel));
    relpointer->segmentId = rs->getID();
    
    relpointer->nameStart = headerpointer->heapStart - (rel.name.size() + 1);
    relpointer->nameLength = rel.name.size() + 1; // including terminating '\0' character
    headerpointer->heapStart -= (rel.name.size() + 1);
    char * namepointer = reinterpret_cast<char *>(static_cast<char *>(page.getData()) + headerpointer->heapStart);
    strcpy(namepointer, rel.name.c_str());
    
    relpointer->pkStart = headerpointer->heapStart - rel.primaryKey.size() * sizeof(unsigned);
    relpointer->pkCount = rel.primaryKey.size();
    headerpointer->heapStart -= rel.primaryKey.size() * sizeof (unsigned);
    unsigned * pkpointer = reinterpret_cast<unsigned *>(static_cast<char *>(page.getData()) + headerpointer->heapStart);
    const unsigned * pks = &rel.primaryKey[0];
    for (unsigned int i = 0; i < rel.primaryKey.size(); i++) {
      *pkpointer = pks[i];
      pkpointer++;
    }

    // create RegularSegment for B-Tree-Index and store its Id
    unique_ptr<RegularSegment> rs2 = segmentInventory.createSegment(1,1);
    relpointer->pkId = rs2->getID();
    
    relpointer->attrStart = headerpointer->heapStart - rel.attributes.size()*sizeof(MetadataStructs::Attribute);
    relpointer->attrCount = rel.attributes.size();
    headerpointer->heapStart -= rel.attributes.size()*sizeof(MetadataStructs::Attribute);
    MetadataStructs::Attribute * attrpointer = reinterpret_cast<MetadataStructs::Attribute *>(static_cast<char *>(page.getData()) + headerpointer->heapStart);

    // for each attribute: set metadata
    for (unsigned i = 0; i < rel.attributes.size(); i++) {
      attrpointer->id = i; // equals order of attributes in list
      attrpointer->type = rel.attributes[i].type;
      attrpointer->charLength = rel.attributes[i].len;
      attrpointer->notNull = rel.attributes[i].notNull;
      attrpointer->nameStart = headerpointer->heapStart - (rel.attributes[i].name.size() + 1);
      attrpointer->nameLength = rel.attributes[i].name.size() + 1;
      headerpointer->heapStart -= (rel.attributes[i].name.size() + 1);
      namepointer = reinterpret_cast<char *>(static_cast<char *>(page.getData()) + headerpointer->heapStart);
      strcpy(namepointer, rel.attributes[i].name.c_str());
      attrpointer += 1;
    }
  }
  bufferManager.unfixPage(page, true); // unlock dirty page
}

unsigned int Metadata::lookup(string& relationName) {
  // get page
  unsigned int pageNo = list.front().startPage;
  BufferFrame& page = bufferManager.fixPage(pageNo, false); // lock shared
  
  MetadataStructs::Header * headerpointer = static_cast<MetadataStructs::Header *>(page.getData());

  for (unsigned int i = 0; i < headerpointer->relCount; i++) {
    MetadataStructs::Relation * relpointer = reinterpret_cast<MetadataStructs::Relation *>(static_cast<char *>(page.getData()) + sizeof(MetadataStructs::Header)) + i;
    char * namepointer = reinterpret_cast<char *>(static_cast<char *>(page.getData()) + relpointer->nameStart);
    char * refnamepointer = (char *) relationName.c_str();
    bool equal = true;
    unsigned int boundary = !(static_cast<unsigned int>(relationName.size()+1)<relpointer->nameLength)?relpointer->nameLength:static_cast<unsigned int>(relationName.size()+1); // compute minimum length of relation name and parameter name.

    for (unsigned int k = 0; k < boundary; k++) {
      if (namepointer[k] != refnamepointer[k])
        equal = false;
    }
    if (equal) {
      bufferManager.unfixPage(page, false);
      return relpointer->segmentId;
    }
  }

  // no match found
  bufferManager.unfixPage(page, false);
  return 4294967295; // maximum value for 4 byte unsigned int // TODO use exception instead?
};

unique_ptr<Relation> Metadata::getRelation(unsigned int relationId) {
   // get page
  unsigned int pageNo = list.front().startPage;
  BufferFrame& page = bufferManager.fixPage(pageNo, false); // lock shared
  
  MetadataStructs::Header * headerpointer = static_cast<MetadataStructs::Header *>(page.getData());

  for (unsigned int i = 0; i < headerpointer->relCount; i++) {
    MetadataStructs::Relation * relpointer = reinterpret_cast<MetadataStructs::Relation *>(static_cast<char *>(page.getData()) + sizeof(MetadataStructs::Header)) + i;
    if (relpointer->segmentId == relationId) {
      // Relation found

      // name
      char * namepointer = reinterpret_cast<char *>(static_cast<char *>(page.getData()) + relpointer->nameStart);
      std::string name(namepointer); // works because of delimination '\0' character

      // attributes
      std::vector<Schema::Relation::Attribute> attributes;
      MetadataStructs::Attribute * attrpointer = reinterpret_cast<MetadataStructs::Attribute *>(static_cast<char *>(page.getData()) + relpointer->attrStart);
      for (unsigned i = 0; i < relpointer->attrCount; i++) {
        Schema::Relation::Attribute * attr = new Schema::Relation::Attribute();
        char * attrnamepointer = reinterpret_cast<char *>(static_cast<char *>(page.getData()) + attrpointer->nameStart);
        std::string attrname(attrnamepointer); // works because of delimination '\0' character
        attr->name = attrname;
        attr->type = attrpointer->type;
        attr->len = attrpointer->charLength;
        attr->notNull = attrpointer->notNull;
        attributes.push_back(*attr);
        
        attrpointer += 1;
      }

      // primaryKey
      std::vector<unsigned> primaryKey;
      unsigned * pkpointer = reinterpret_cast<unsigned *>(static_cast<char *>(page.getData()) + relpointer->pkStart);
      for (unsigned int i = 0; i < relpointer->pkCount; i++) {
        primaryKey.push_back(pkpointer[i]);
      }

      // primaryKeySegmentId
      unsigned primaryKeySegmentId = relpointer->pkId;
      
      Relation * tmp = new Relation(name); //TODO convert real data
      tmp->attributes = attributes;
      tmp->primaryKey = primaryKey;
      tmp->primaryKeySegmentId = primaryKeySegmentId;
      bufferManager.unfixPage(page, false);
      return unique_ptr<Relation>(tmp);
    }
  }
  bufferManager.unfixPage(page, false);
  return 0;
};

