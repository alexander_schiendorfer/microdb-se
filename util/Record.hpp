#ifndef H_Record_HPP
#define H_Record_HPP

#include <string.h>
#include <cstdlib>

// A simple Record implementation (without custom allocator and error handling)
/**
 * Records are persisted by using len and storing len bytes
 * on page
 */
class Record {
   unsigned len;
   char* data;

public:
   Record& operator=(Record& rhs) = delete;
   Record(Record& t) = delete;
   Record(Record&& t) : len(t.len), data(t.data) {
      t.data = 0;
      t.len = 0;
   }
   explicit Record(unsigned len, const char* const ptr) : len(len) {
       // memory is allocated directly in record, sequentially
	   data = static_cast<char*>(malloc(len));
	   memcpy(data, ptr, len);
   }

   const char* getData() const {
      return data;
   }

   unsigned getLen() const {
      return len;
   }

   ~Record() {
     // no need to free since no
	   free(data);
   }
};

#endif
