#include "Util.h"
#include <iostream>
#include <stdint.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <bitset>

bool Util::isSorted(int fd) {
   struct stat fileStat;
   int fileSize;
   bool sorted = true;
   
   fstat(fd, &fileStat);
   fileSize = fileStat.st_size;
   
   char* buffer; // managed by mmap
   buffer = (char *) mmap(NULL, fileSize, PROT_READ, MAP_FILE | MAP_SHARED, fd, 0);
   
   if (buffer == MAP_FAILED) { // something went terribly wrong
     std::cout << "error occured " << std::endl;
     return false;
   }
   unsigned long size = fileSize / sizeof(uint64_t); 
   unsigned long* castPtr = (unsigned long*) buffer;
   unsigned long* endPtr = castPtr + size;
   
   unsigned long lastNumber = 0;
   while( castPtr < endPtr) {
			unsigned long newNumber = *castPtr;
			if(newNumber < lastNumber) {
				sorted = false;
			} 
		//	std::cout << newNumber << std::endl;
			lastNumber = newNumber;
			castPtr += 1;
   } 
   
   munmap(buffer, fileSize);
   return sorted;   
}

TID Util::getTID(unsigned pageId, unsigned short slotId) {
	TID newTid;
	newTid.pageId = pageId;
	newTid.slotId = slotId;
	return newTid;
}


