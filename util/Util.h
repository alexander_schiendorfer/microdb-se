#ifndef DBS_UTIL_H
#define DBS_UTIL_H
#include <unistd.h>
#include "tid.h"

/**
@brief Util class for file operations

This Util will grow and grow
*/
class Util {

  public:
  
    /** @brief checks if a file is sorted (containing uint64) based on a fd
    * @param fd file descriptor of file to check; file contains uint64 numbers
    */
    static bool isSorted(int fd);

    /**
     * Creates a valid TID using 28 bit of pageId and 12 bit of slotId
     */
    static TID getTID(unsigned pageId,unsigned short slotId);
};
#endif
