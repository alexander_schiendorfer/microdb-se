/*
 * tid.h
 *
 * Header file containing type definitions for
 * TIDs
 *  Created on: May 13, 2012
 *      Author: Alexander Schiendorfer
 */

#ifndef TID_H_
#define TID_H_

typedef struct TID {
    unsigned int pageId;
    unsigned short slotId;
} TID;


#endif /* TID_H_ */
