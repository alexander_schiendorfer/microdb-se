#include "RegularSegment.h"
#include "SegmentInventory.h"
#include <list>
#include <algorithm>
#include <iostream>
// is regular segment 

RegularSegment::RegularSegment(BufferManager& bm, int id, std::list<ExtentListEntry>& list, SegmentInventory& si) : Segment(bm, id), list(list), segmentInventory(si) {
	unsigned endId = 0;
    size = 0;
	// create addressable space
	for(auto it = list.begin(); it != list.end(); it++) {
		AddressableEntry newEntry;
		newEntry.extent = *it;
		endId += (*it).length;
		newEntry.lastIndex = endId - 1; // creates logical IDs
		// adding addressable entry
		addressableEntries.push_back(newEntry);
		size += (*it).length;
	}
}

unsigned int RegularSegment::getSize() const {
    return size;
}

unsigned RegularSegment::grow() {
    return segmentInventory.growSegment(this->getID());
}

std::list<ExtentListEntry> RegularSegment::getExtentList() {
    return list;
}

std::list<ExtentListEntry>::iterator RegularSegment::getExtentListIterator() {
    return list.begin();
}

unsigned RegularSegment::operator [](unsigned id) const {
	AddressableEntry dummy;
	dummy.lastIndex = id;

	auto cmp = [](const AddressableEntry& a1, const AddressableEntry& a2) {
		return a1.lastIndex < a2.lastIndex;
	};

	auto extent = lower_bound(addressableEntries.begin(), addressableEntries.end(), dummy, cmp);
	//std::cout << "For id ... "<< id << " I found extent: " << (*extent).extent.startPage << " / " << (*extent).lastIndex << std::endl;
	unsigned logicalStart = (*extent).lastIndex - (*extent).extent.length + 1;
	unsigned diff = id - logicalStart ;
	return (*extent).extent.startPage + diff ;
}
