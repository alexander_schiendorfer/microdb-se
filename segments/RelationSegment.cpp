/*
 * RelationSegment.cpp
 *
 *  Created on: Jun 12, 2012
 *      Author: alexander
 */

#include "RelationSegment.h"
#include "../operators/Register.h"
#include "SPSegment.h"
#include <string>


RelationSegment::RelationSegment(BufferManager& bm, int id, std::list<ExtentListEntry> & list, SegmentInventory& si, Schema::Relation* relation, bool initialize): SPSegment(bm, id, list, si, initialize), relation(relation) {
	bufferLen = 0;
	for(auto it = relation->attributes.begin(); it != relation->attributes.end(); it++) {
		switch(it->type) {
		case Types::Tag::Integer:
			bufferLen += sizeof(int);
			break;
		case Types::Tag::Char:
			bufferLen += it->len;
			break;
		}
	}
}

RelationSegment::~RelationSegment() {
	// TODO Auto-generated destructor stub
}

TID RelationSegment::insertTuple(std::vector<unique_ptr<Register> >& tuple) {
	char buffer[bufferLen];
	char* bufferPos = buffer;

	auto attrIt = relation->attributes.begin();
	for(auto it = tuple.begin(); it != tuple.end(); it++) {
		Register* tmp = it->get();
		const Schema::Relation::Attribute& attr = *attrIt;

		if (tmp->isInt()) {
			*(reinterpret_cast<int*>(bufferPos)) = tmp->getInteger();
			bufferPos += sizeof(int);
		} else {
			unsigned bufferIncr = attr.len;
			memcpy(bufferPos, tmp->getString().c_str(), tmp->getString().length()+1); // including null byte
			bufferPos += bufferIncr;
		}
		attrIt++;
	}

	Record r(bufferLen, buffer);
	// insert into SpSegment
	TID tid = insert(r);
	return tid;
}
