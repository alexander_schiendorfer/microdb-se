#ifndef REGULAR_SEGMENT_H
#define REGULAR_SEGMENT_H

#include <list>
#include "Segment.h"
#include "Structs.h"

class SegmentInventory; // forward declaration for cyclic reference

class RegularSegment : public Segment { 
  protected:
    std::list<ExtentListEntry> list;
    std::list<AddressableEntry> addressableEntries;
    SegmentInventory& segmentInventory;
    unsigned size;
    unsigned grow();

  public:
    /**
     * 
     */
    RegularSegment (BufferManager& bm, int id, std::list<ExtentListEntry> & list, SegmentInventory& si);

    /**
     * Returns size in pages.
     */
    unsigned int getSize() const;

    /**
     * Returns redundant list of extents.
     */
    std::list<ExtentListEntry> getExtentList();

    /**
     * Returns iterator (list.begin()).
     */
    std::list<ExtentListEntry>::iterator getExtentListIterator();

    /**
     * Enables linear addressing -> retrieves pages by "logical" id
     * E.g. segments 2-5, 13-15 allocated:
     * seg[0] -> 2, seg[3] = 5, seg[4] = 13 and so on
     */
     unsigned operator[](unsigned id) const;
};

#endif
