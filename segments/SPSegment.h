#ifndef SP_SEGMENT_H
#define SP_SEGMENT_H

#include "RegularSegment.h"
#include "../util/tid.h"
#include "../util/Record.hpp"
#include "../buffer/BufferManager.h" // for PAGESIZE
#include <list>
#include "Structs.h"

/**
 * Class representing a slotted page segment in main
 * memory.
 *
 * Provides methods for writing proper SP headers and format slotted
 * pages as well as the interface for record manipulation
 *
 * All TIDs passed in arguments can be public TIDs or TID's that occur
 * in case of indirection -> for recursive use, needs "wasMoved" semantics
 */
class SPSegment : public RegularSegment { 
private:

  /**
   * Maximal size of a single tuple
   */
  const unsigned maxTupleSize = PAGESIZE - sizeof(SPHeader) - sizeof(Slot);

  /**
   * Currently locked pages by segment
   */
  std::list<unsigned> lockedPages;

  BufferFrame& findFreePage(unsigned neededSpace);

  TID insertInternally(const Record& r, TID* oldTid);
  /**
   * writes initial header into frame
   */
  void writeEmptyHeader(BufferFrame& frame);

  /**
   * Private insert method for standard tuple insert at the end
   * expects already locked frame
   */
  void insertEnd(BufferFrame& frame, const Record& r, TID& tid, Slot* slot, TID* oldTid);

  /**
   * Finds the first unused slot (if any exist) or returns a new slot
   * at the end and increments slot counter
   */
  Slot* findUnusedSlot(SPHeader& header);

  /**
   * Compactifies by changing existing pointers of buffer slots
   * cannot touch unused slots
   * expects already locked frame
   */
  void compactify(BufferFrame& frame);

  /**
   * Performs internal update operation bearing in mind indirection
   * if origin is NULL, indirection will be added, else changed
   * hence kept at only one indirection step
   */
  bool updateInternally(TID tid, const Record& r, ReferenceSlot* originPage);

public:
  SPSegment(BufferManager& bm, int id, std::list<ExtentListEntry> & list, SegmentInventory& si, bool initialize);


  /**
   * Inserts a new record into the segment and returns a TID
   * might reuse TIDs from unused slots
   */
  TID insert(const Record& r);

  /**
   * deletes a record with a given TID (invalidates this slot)
   * returns false if TID was already deleted
   * @throws runtime_error if invalid TID is passed
   */
  bool remove(TID tid);

  /**
   * loads a record from a given TID
   * user is required to call delete on the returned record manually (or using unique_ptr ...)
   * @throws runtime_error if invalid TID is passed
   */
  Record* lookup(TID tid);

  bool update(TID tid, const Record& r);

};

#endif
