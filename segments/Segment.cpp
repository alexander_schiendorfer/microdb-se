// implementation file for Segments
#include "Segment.h"

Segment::Segment(BufferManager& bm, int id) : id(id),  bufferManager(bm) {
}

int Segment::getID() {
    return id;
}
