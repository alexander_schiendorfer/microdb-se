/*
 * RelationSegment.h
 *
 *  Created on: Jun 12, 2012
 *      Author: alexander
 */

#ifndef RELATIONSEGMENT_H_
#define RELATIONSEGMENT_H_

#include "SPSegment.h"
#include "../schema/Schema.h"
#include "../operators/Register.h"
#include <vector>
#include <memory>

class RelationSegment: public SPSegment {
private:
	Schema::Relation* relation;
	unsigned int bufferLen;

public:
	RelationSegment(BufferManager& bm, int id, std::list<ExtentListEntry> & list, SegmentInventory& si, Schema::Relation* relation, bool initialize);
	virtual ~RelationSegment();
	TID insertTuple(std::vector<unique_ptr<Register> >& tuple);

	// additional helpers to work with records as tuples

};

#endif /* RELATIONSEGMENT_H_ */
