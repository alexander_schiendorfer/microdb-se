#ifndef STRUCTS_H
#define STRUCTS_H
#include "../util/tid.h"

// structure of data: HEADER, EXTENTLIST, <free space>, FREELISTE

struct Header {
    unsigned int extentCount; // number of ExtentListEntries
    unsigned int freeSpace; // sum of free pages (= not assigned to a segment)
    unsigned int freeListCount; // number of FreeListEntries
};

struct ExtentListEntry {
    unsigned int segmentID;
    unsigned int startPage;
    unsigned int length; // in pages
};

struct AddressableEntry {
	ExtentListEntry extent;
	unsigned int lastIndex;
};

struct FreeListEntry {
    unsigned int startPage;
    unsigned int length; // in pages
};

struct Slot {
	char isReference;  // indicates whether to use ReferenceSlot
	char wasMoved;     // 1 if page was moved from another page
	unsigned short offset;
	unsigned short length;
	char unused;        // to equalize binary layout
};

/**
 * Slotted page header
 */
struct SPHeader {
	unsigned LSN;
	unsigned slotCount;
	unsigned unusedSlots;
	Slot* firstSlot;
	void* dataStart;
	unsigned freeSpace;
};

struct ReferenceSlot {
	char isReference;
	TID tid;
};

#endif
