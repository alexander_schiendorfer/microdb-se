#include "SPSegment.h"
#include "../util/tid.h"
#include "../util/Record.hpp"
#include "../buffer/BufferFrameHolder.h"
#include "Structs.h"
#include <iostream>
#include <list>
#include <cstdlib> // for memcpy
#include <algorithm>  // for find
#include <stdexcept>  // for runtime_error
#include <set>

using namespace std;

SPSegment::SPSegment(BufferManager& bm, int id, std::list<ExtentListEntry> & list, SegmentInventory& si, bool initialize) : RegularSegment(bm, id, list, si) {
  // formatting all buffer frames as slotted pages

  // preparing header for empty slotted page
  if(initialize) { // first access on segment
	  for(auto it = list.begin(); it != list.end(); it++) {
		  ExtentListEntry& entry = *it;

		  for(unsigned i = entry.startPage; i < entry.startPage + entry.length; i++) {
			  BufferFrameHolder holder(bm, i, true);
			  writeEmptyHeader(holder.getProtectedFrame());
		  }
	   }
  }
}

void SPSegment::writeEmptyHeader(BufferFrame& frame) {
	SPHeader sh;
	sh.LSN = 0;
	sh.slotCount = 0;
	sh.unusedSlots = 0;
	sh.freeSpace = PAGESIZE - sizeof(SPHeader);

	// initially points to the end
	sh.dataStart = static_cast<char*>(frame.getData()) + PAGESIZE;
	// slots start directly after header
	// using char* to avoid pointer arithmetics with void*
	sh.firstSlot = reinterpret_cast<Slot*>(static_cast<char*>(frame.getData()) + sizeof(SPHeader));

	// copy header to first bytes of page
	*(static_cast<SPHeader*>(frame.getData())) = sh;
}

/**
 * Does not compactify yet!
 */
BufferFrame& SPSegment::findFreePage(unsigned neededSpace) {
	// TODO implement FSI

	// we need to watch out, might already lock a page while iterating
	for(auto it = list.begin(); it != list.end(); it++) {
		ExtentListEntry& extent = *it;
		for(unsigned page = extent.startPage; page < extent.startPage + extent.length; page++ ) {
			if(find(lockedPages.begin(), lockedPages.end(), page) != lockedPages.end()) // already locked
				continue;
			BufferFrame& frame = bufferManager.fixPage(page, true);
			lockedPages.push_back(page);

			SPHeader& header =  *(static_cast<SPHeader*>(frame.getData()));
			if(header.freeSpace >= neededSpace)
				return frame;
			lockedPages.remove(page);
			bufferManager.unfixPage(frame, false);
		}
	}

	// if we reached this point, we couldn't find space so we need to grow!
	if(grow() == 0) {
		throw runtime_error("Stuck in finding room for a tuple and grow returned 0.");
	} else  {
		// another try with new pages
		return findFreePage(neededSpace);
	}
}

TID SPSegment::insertInternally(const Record& r, TID* oldTid) {
	unsigned neededSpace = r.getLen() + sizeof(Slot);
	if(oldTid != NULL)
		neededSpace += sizeof(TID);

	if(neededSpace - sizeof(Slot) > maxTupleSize)
		throw runtime_error("Tuple is too large! (Max Size " + maxTupleSize);

	// grows if necessary
	BufferFrame& frame = findFreePage(neededSpace);
	TID newTid;
	insertEnd(frame, r, newTid, NULL, oldTid);

	unsigned page = frame.getPageNo();
	bufferManager.unfixPage(frame, true);
	lockedPages.remove(page);
	return newTid;
}

TID SPSegment::insert(const Record& r) {
	return insertInternally(r, NULL);
}

Slot* SPSegment::findUnusedSlot(SPHeader& header) {
  Slot* freeSlot;
  if(header.unusedSlots == 0) { // create a new slot
     freeSlot = header.firstSlot + header.slotCount;
     header.slotCount++;
  } else {
     freeSlot = header.firstSlot;
     while((freeSlot->isReference == 1 || !freeSlot->unused) && freeSlot - header.firstSlot <= header.slotCount) {
    	 freeSlot++;
     }
     if(freeSlot->isReference == 1 || !freeSlot->unused)
    	 throw runtime_error("Logical error, marked free slots but did not found one");
     header.unusedSlots--;
  }
  return freeSlot;
}

void SPSegment::insertEnd(BufferFrame& frame, const Record& r, TID& tid, Slot* slot, TID* oldTid) {
	SPHeader& header = *(static_cast<SPHeader*>(frame.getData()));
    unsigned neededSpace = 0 ;
    unsigned payLoadSpace = r.getLen();

    // maybe we need to store the old TID for a record
    if(oldTid != NULL) {
    	payLoadSpace += sizeof(TID);
    }
    neededSpace += payLoadSpace;
    // if we update a record we can assign the slot beforehand (to keep TID consistent)

    // else we need to find an unused one which might result in an extension of the slot list
    if(slot == NULL) {
    	if(header.unusedSlots == 0) // will need a new slot
    		neededSpace += sizeof(Slot);

    	slot = findUnusedSlot(header);
    	tid.slotId = slot - header.firstSlot;
    	tid.pageId = frame.getPageNo();
    }

	char* dataStart = static_cast<char*>(header.dataStart);
	char* slotEnd = reinterpret_cast<char*>(header.firstSlot + header.slotCount);

	if(dataStart - slotEnd < payLoadSpace) {  // not enough space available at the end!
		compactify(frame);
	}

	// maybe a bit crude but we would get notified by such a nasty bug
	dataStart = static_cast<char*>(header.dataStart);
	slotEnd = reinterpret_cast<char*>(header.firstSlot + header.slotCount);

	if(dataStart - slotEnd < payLoadSpace)
		throw runtime_error("Implementation error while compactifying");

	slot->isReference = 0; // must not be called with a reference slot
	slot->length = (oldTid == NULL) ? r.getLen() : sizeof(TID) + r.getLen();
	slot->offset = (dataStart - payLoadSpace) - static_cast<char*> (frame.getData());

	slot->wasMoved = (oldTid == NULL) ? 0 : 1;
	slot->unused = 0;

	// found position to insert
	char* tuplePtr = static_cast<char*>(frame.getData()) + slot->offset;
	if(oldTid != NULL) { // copy TID at start of record
		memcpy(tuplePtr, oldTid, sizeof(TID));
		tuplePtr += sizeof(TID);
	}
	memcpy(tuplePtr, r.getData(), r.getLen());

	// update header of page where inserted
	header.freeSpace -= neededSpace;
	header.dataStart = static_cast<void*>(tuplePtr);
}

void SPSegment::compactify(BufferFrame& frame) {
	SPHeader& header = *static_cast<SPHeader*>(frame.getData());
	char* frameStart = static_cast<char*>(frame.getData());
    char* dataEnd = frameStart + PAGESIZE - 1; // last writable byte

    // get ordered set of tuples using lambda expression descendingly!
    bool (*funcPtr)(Slot*,Slot*) = [](Slot* first, Slot* second) { return second->offset < first->offset; };
    std::set<Slot*, bool (*)(Slot*, Slot*)> slotsOrdered(funcPtr);

    Slot* currentSlot = header.firstSlot;
    for(unsigned i = 0; i < header.slotCount; i++) {
    	if(!currentSlot->unused) {
    		slotsOrdered.insert(currentSlot);
    	}
    	currentSlot++;
    }

    for(auto it = slotsOrdered.begin(); it != slotsOrdered.end(); it++) {
    	Slot* currentSlot = *it;
    	char* tupleStart = frameStart + currentSlot->offset;
    	char* tupleEnd = tupleStart + currentSlot->length - 1; // last byte of tuple
    	if(tupleEnd != dataEnd) { // we have something to do here
    		while(tupleEnd >= tupleStart) {
    			*dataEnd = *tupleEnd;
    			dataEnd--; tupleEnd--;
    		}
    		currentSlot->offset = dataEnd - frameStart + 1;
    	} else { // set pointers appropriately
    		dataEnd = tupleStart - 1; // points to first byte not in previous tuple
    	}
    }

    // update Header
    header.dataStart = static_cast<void*>(dataEnd);
}

Record* SPSegment::lookup(TID tid) {
	// TODO should we implement a check whether this buffer frame (tid.pageId) is actually ours?
	BufferFrameHolder holder(bufferManager, tid.pageId, true, &lockedPages);
	BufferFrame& frame = holder.getProtectedFrame();

	SPHeader& header = *(static_cast<SPHeader*>(frame.getData()));
	if(tid.slotId > header.slotCount)
		throw runtime_error("invalid tid "); // invalid TID

	Slot& slot = header.firstSlot[tid.slotId];

	if(slot.unused)
		throw runtime_error("Error. Accessing deleted tuple.");

	Record* newRecord = NULL;
	if(slot.isReference) { // perform recursive lookup
		// cast to reference slot type to get TID
		ReferenceSlot& refSlot = *reinterpret_cast<ReferenceSlot*>(&slot);
		newRecord = lookup(refSlot.tid);
	} else { // wanted tuple is on this page
		char* tuplePtr = static_cast<char*>(frame.getData()) + slot.offset;
		unsigned short actualLength = slot.length;
		if(slot.wasMoved) {
			tuplePtr += sizeof(TID); // offset due to indirection
			actualLength -= sizeof(TID);
		}
		newRecord = new Record(actualLength, tuplePtr);
	}


	return newRecord;
}

bool SPSegment::remove(TID tid) {
	BufferFrameHolder holder(bufferManager, tid.pageId, true, &lockedPages);
	BufferFrame& frame = holder.getProtectedFrame();

	SPHeader& header = *(static_cast<SPHeader*>(frame.getData()));
	if(tid.slotId > header.slotCount)
		throw runtime_error("invalid tid "); // invalid TID

	Slot& slot = header.firstSlot[tid.slotId];
	if(slot.isReference) { // inherently !unused since isReference is still set
		ReferenceSlot& refSlot = *reinterpret_cast<ReferenceSlot*>(&slot);
		// perform recursive delete
		remove(refSlot.tid);
		slot.isReference = slot.wasMoved = 0;
		slot.unused = 1;
		header.unusedSlots++;
		slot.offset = slot.length = 0;
		return true;
	} else { // tuple is on same page
		if(!slot.unused) {
			slot.unused = 1;
			header.unusedSlots++;
			header.freeSpace += slot.length;
			// if this was dataStart -> update header.dataStart
			char* tuplePtr = static_cast<char*>(frame.getData()) + slot.offset;
			if(static_cast<char*>(header.dataStart) == tuplePtr) {
				header.dataStart = tuplePtr + slot.length;
			}
			slot.offset = slot.length = slot.wasMoved = 0;
			return true;
		} else
			return false;
	}
}

bool SPSegment::updateInternally(TID tid, const Record& r, ReferenceSlot* originSlot) {
	BufferFrameHolder holder(bufferManager, tid.pageId, true, &lockedPages);
	BufferFrame& frame = holder.getProtectedFrame();
	SPHeader& header = *(static_cast<SPHeader*>(frame.getData()));
	ReferenceSlot* refSlot;

	if(tid.slotId > header.slotCount)
		throw runtime_error("invalid tid ");

	Slot& slot = header.firstSlot[tid.slotId];
	if(!slot.isReference) {
		if(r.getLen() <= slot.length) { // everything is good, we can now only update
			memcpy(static_cast<char*>(frame.getData()) + slot.offset, r.getData(), r.getLen());
			// update free space according to delta
			unsigned delta = slot.length - r.getLen();
			header.freeSpace += delta;
			slot.length = r.getLen();

		} else {
			// try to find space on same page

			if(header.freeSpace + slot.length >= r.getLen()) {
				slot.unused = 1; // for possible compactify
				header.freeSpace += slot.length;
				insertEnd(frame, r, tid, &slot, NULL);
				// insertEnd sets unused to 0 again
			} else {
				// find new space on different page
				TID newTid = insertInternally(r, &tid);
				if(originSlot != NULL) { // I am in a referenced TID
					slot.unused = 1; // slot of this page available again
					header.freeSpace += slot.length;
					slot.isReference = slot.offset = slot.length = 0;
					// set indirection at origin
					originSlot->isReference = 1;
					originSlot->tid = newTid;
				} else {
					// mark previously used space as free
					header.freeSpace += slot.length;
					slot.isReference = 1;
					refSlot = reinterpret_cast<ReferenceSlot*>(&slot);
					refSlot->tid = newTid;
				}
			}
		}
	} else {
		refSlot = reinterpret_cast<ReferenceSlot*>(&slot);
		if(header.freeSpace >= r.getLen()) { // now there's enough space again
			remove(refSlot->tid);
			insertEnd(frame, r, tid, &slot, NULL);
		} else {
			updateInternally(refSlot->tid, r, refSlot);
		}
	}
	return true;
}

bool SPSegment::update(TID tid, const Record& r) {
	return updateInternally(tid, r, NULL);
}
