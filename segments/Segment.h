#ifndef SEGMENT_H
#define SEGMENT_H

#include "../buffer/BufferManager.h"

class Segment  {
  protected:
    int id;
    BufferManager& bufferManager;       // segments need to access buffer manager for pages

  public:

    Segment(BufferManager& bm, int id);

    /**
     * Returns SegmentID;
     */
    int getID();

};

#endif
