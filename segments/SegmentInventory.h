#ifndef SEGMENT_INVENTORY_H
#define SEGMENT_INVENTORY_H
#include "RegularSegment.h"
#include "../buffer/BufferManager.h"
#include "../schema/Types.h"
#include "../schema/Schema.h"
#include "Structs.h"
#include "SPSegment.h"
#include "RelationSegment.h"
#include <memory>
#include <iostream>
#define GROWTH 1.25

class SegmentInventory : public Segment { 
  private: 

    BufferFrame& bufferFrame;
    Header * header;
    ExtentListEntry * extentListPointer;

    // private helper methods
    /**
     * Searches for free pages to allocate. If there is enough space for 'max': search for a slice that is >= 'max'. If none is found or if there is less than 'max' space, greedy take all slices of free pages. Throws an exception is there is less than 'min' free space.
     */
    unsigned allocate(unsigned int min, unsigned int max, unsigned int id);

    /**
     * Determines weather there is alreadys a segment with the given id or not
     *@param id
     */
    bool isSegmentIdUsed(unsigned int id);

    /**
     * Creates an entry in extentList and updates freeSpaceList.
     * @param id
     */
    void createExtent(unsigned int id, FreeListEntry * entry, unsigned int availableLength, unsigned int usedLength);

    /**
     * Updates freeList (e.g. merging free intervals).
     */
    void deleteExtent(ExtentListEntry * entry);

    /**
     * Used by createSegment and growSegment
     */
    unique_ptr<RegularSegment> createSegmentWithID(unsigned int min, unsigned int max, int id);

  public: 
    /**
     * Uses the BufferManager to get access to page 0
     */
    SegmentInventory(BufferManager& bm);

    ~SegmentInventory();

    /**
     * page 0 has to get proper layout (header etc) if not yet initialized
     *
     * Install is necessary to be a separate method since DB shouldn't be reset
     * at every start of DBMS
     */
    void install();

    /**
     * Creates a new segment by allocating memory for extents ranging
     * from min to max
     */
    unique_ptr<RegularSegment> createSegment(unsigned int min, unsigned int max);

    /**
     * Creates a new segment by allocating memory for extents ranging
     * from min to max
     */
    unsigned createSegmentGiveId(unsigned int min, unsigned int max);

    /**
     * 
     */
    void dropSegment(unsigned int id);

    /**
     * Uses GROWTH as factor.
     * Returns the number of pages grown
     */
    unsigned growSegment(unsigned int id);

    /**
     * returns a segment as regular segment only
     */
    unique_ptr<RegularSegment> retrieveSegment(unsigned int id);

    /**
    * returns a segment as spsegment
    */
    unique_ptr<SPSegment> retrieveSPSegment(unsigned int id);

    /**
     * Returns segment with extent list as relation segment, call
     * create segment beforehand -> TODO should be replaced by template
     */
    unique_ptr<RelationSegment> retrieveRelationSegment(unsigned int id, Schema::Relation* relation);

    /**
     * Performs the same actions as retrieveXYZSegment but does not initialize
     * so no mandatory overriding
     */
    unique_ptr<RelationSegment> getRelationSegment(unsigned int id, Schema::Relation* relation, bool initialize);
    unique_ptr<SPSegment> getSPSegment(unsigned int id, bool initialize);
};

#endif
