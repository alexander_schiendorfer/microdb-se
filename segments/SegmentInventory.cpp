// segment inventory verwaltet sich selbst
#include "SegmentInventory.h"
#include "../buffer/BufferManager.h" // for PAGESIZE
#include "RegularSegment.h"
#include "SPSegment.h"
#include <stdexcept>  // for runtime_error
#include <iostream>
#include <list>
#include <algorithm> // for max
#include <memory>    // for unique_ptr

using namespace std;

SegmentInventory::SegmentInventory(BufferManager& bm) : Segment(bm, -1), bufferFrame(bufferManager.fixPage(0, true))  {
    // fix first page for SI, hold while program is executed
    // Place the header pointer to the beginning of the page data
    header = static_cast<Header *>(bufferFrame.getData());
    // The extendList starts right after the header
    extentListPointer = reinterpret_cast<ExtentListEntry *>(static_cast<Header *>(bufferFrame.getData()) + 1);
}

SegmentInventory::~SegmentInventory()  {
    bufferManager.unfixPage(bufferFrame, true); // unfix first page when program execution ends
}

void SegmentInventory::install() {
    // create header
    Header* header = static_cast<Header *>(bufferFrame.getData());
    header->extentCount = 0;
    header->freeSpace = bufferManager.getSize()-1;
    header->freeListCount = 1;

    // create freeList entry
    FreeListEntry * freeListPointer = reinterpret_cast<FreeListEntry *>(static_cast<char *>(bufferFrame.getData()) + PAGESIZE) - header->freeListCount;
    freeListPointer->startPage = 1;
    freeListPointer->length = (bufferManager.getSize() - 1);

}

unique_ptr<RegularSegment> SegmentInventory::createSegment(unsigned int min, unsigned int max) {
    // find free SegmentID
    id = 0;
    for (unsigned int i = 0; i < header->extentCount; ++i) {
		ExtentListEntry entry = extentListPointer[i];
		int tmpSegmentID =  static_cast<int>(entry.segmentID);
		if (id == tmpSegmentID) {
			// segmentID is already used, try next one
			++id;
		}
    }
    // segmentID is not used, we can use it for the new segment
    
    return createSegmentWithID(min, max, id);
}

unsigned SegmentInventory::createSegmentGiveId(unsigned int min, unsigned int max) {
    // find free SegmentID
    id = 0;
    for (unsigned int i = 0; i < header->extentCount; ++i) {
		ExtentListEntry entry = extentListPointer[i];
		int tmpSegmentID =  static_cast<int>(entry.segmentID);
		if (id == tmpSegmentID) {
			// segmentID is already used, try next one
			++id;
		}
    }
    // segmentID is not used, we can use it for the new segment
    
    createSegmentWithID(min, max, id);
    return id;
}

bool SegmentInventory::isSegmentIdUsed(unsigned int id) {
    std::list<ExtentListEntry> list;
    for (unsigned int i = 0; i < header->extentCount; ++i) {
        if (extentListPointer[i].segmentID == id) {
		return true;
        }
    }
    return false;
}

void SegmentInventory::dropSegment(unsigned int id) {
    // for each extent of this segment
    std::list<ExtentListEntry> list;
    std::list<unsigned int> entriesToDelete; // ordered from back to front!

    for (unsigned int i = 0; i < header->extentCount; ++i) {
        if (extentListPointer[i].segmentID == id) {
            // do not move entries in extentList while iterating!            
            // mark this entry to be deleted
            entriesToDelete.push_back(i);
            // update freeList
            deleteExtent(extentListPointer + i);
        }
    }

    // now delete the extentListEntries from back to front
    std::list<unsigned int>::iterator i;
    for (i = entriesToDelete.begin(); i != entriesToDelete.end(); i++) {
        // really delete
	for (unsigned int j = (*i); j < header->extentCount; ++j) {
        	// for every successor
		extentListPointer[j] = extentListPointer[j+1];
        }
        // update header
        --header->extentCount;
    }
}

unsigned SegmentInventory::growSegment(unsigned int id) {
    // similar to creatingSegment, but already knows the id
    std::list<ExtentListEntry> list = retrieveSegment(id)->getExtentList();
    std::list<ExtentListEntry>::iterator i;
    
    unsigned int maxExtentSize = 0;
    for (i = list.begin(); i != list.end(); i++) {
        maxExtentSize = max(maxExtentSize, (*i).length);
    }

    unique_ptr<RegularSegment> seg = createSegmentWithID(0, GROWTH*maxExtentSize, id);
    return (*seg).getSize();
}

unique_ptr<RegularSegment> SegmentInventory::retrieveSegment(unsigned int id) {
    std::list<ExtentListEntry> list;
    for (unsigned int i = 0; i < header->extentCount; ++i) {
        if (extentListPointer[i].segmentID == id) {
            list.push_back(extentListPointer[i]);
        }
    }
    return unique_ptr<RegularSegment>(new RegularSegment(bufferManager, id, list, *this));
}

unique_ptr<SPSegment> SegmentInventory::getSPSegment(unsigned int id, bool initialize) {
    Header header = static_cast<Header *>(bufferFrame.getData())[0];
    ExtentListEntry * extentListPointer = reinterpret_cast<ExtentListEntry *>(static_cast<Header *>(bufferFrame.getData()) + 1);
    std::list<ExtentListEntry> list;
    for (unsigned int i = 0; i < header.extentCount; ++i) {
        if (extentListPointer[i].segmentID == id) {
            list.push_back(extentListPointer[i]);
        }
    }
    return unique_ptr<SPSegment>(new SPSegment(bufferManager, id, list, *this, initialize));
}

// TODO refactor and try to use template parameter instead
unique_ptr<SPSegment> SegmentInventory::retrieveSPSegment(unsigned int id) {
	return getSPSegment(id, true);
}

unique_ptr<RelationSegment> SegmentInventory::getRelationSegment(unsigned int id, Schema::Relation* relation, bool initialize) {
  Header header = static_cast<Header *>(bufferFrame.getData())[0];
  ExtentListEntry * extentListPointer = reinterpret_cast<ExtentListEntry *>(static_cast<Header *>(bufferFrame.getData()) + 1);
  std::list<ExtentListEntry> list;
  for (unsigned int i = 0; i < header.extentCount; ++i) {
		if (extentListPointer[i].segmentID == id) {
			list.push_back(extentListPointer[i]);
		}
  }
  return unique_ptr<RelationSegment>(new RelationSegment(bufferManager, id, list, *this, relation, initialize));

}

// TODO refactor and try to use template parameter instead
unique_ptr<RelationSegment> SegmentInventory::retrieveRelationSegment(unsigned int id, Schema::Relation* relation) {
	return getRelationSegment(id, relation, true);
}

unsigned SegmentInventory::allocate(unsigned int min, unsigned int max, unsigned int id) {
    FreeListEntry * freeListPointer = reinterpret_cast<FreeListEntry *>(static_cast<char *>(bufferFrame.getData()) + PAGESIZE) - header->freeListCount;

    if (header->freeSpace >= max) {
         // enough freeSpace for size 'max'
         for (unsigned int i = 0; i < header->freeListCount; ++i) {
             // for each entry in freeList
             FreeListEntry entry = freeListPointer[i];
             if (entry.length >= max) {
                 // free space found
                 ////cout << "insert: extent fits into one slice: startPage " << entry.startPage << endl;
                 createExtent(id, freeListPointer+i, entry.length, max);
                 return max;
             }
         }
         throw runtime_error("Should have been able to find free entry but did not happen");
    } else if (header->freeSpace >= min) {
         // enough freeSpace for size 'min'
         // how to get here: freeSpace is >= max but no slice was big enough   OR   freeSpace is >= min and < max

         // greedy: take all the slices until the are no more or 'max' is reached
         unsigned int newPages = 0; // count how many new pages we allocated
         for (unsigned int i = 0; i < header->freeListCount; ++i) {
             // for each entry in freeList
             FreeListEntry entry = freeListPointer[i];
             if (entry.length >= (max - newPages)) {
                 // last slice to allocate
                 //cout << "insert: greedy: final extent found: startPage " << entry.startPage << endl;
                 createExtent(id, freeListPointer+i, entry.length, (max - newPages));
                 break;
             }
             //cout << "insert: greedy: extent found: startPage " << entry.startPage << endl;
             createExtent(id, freeListPointer+i, entry.length, entry.length);
             newPages += entry.length;
         }
         return newPages;
    } else {
    	// TODO grow file here
        std::cerr << "Error occurred when allocating (id=" << id << ", min=" << min << ", max=" << max << ")" << std::endl;
        throw runtime_error("Not enough space to allocate an extent for segment " + id);
    }
}

void SegmentInventory::createExtent(unsigned int id, FreeListEntry * entry, unsigned int availableLength, unsigned int usedLength) {
    FreeListEntry * freeListPointer = reinterpret_cast<FreeListEntry *>(static_cast<char *>(bufferFrame.getData()) + PAGESIZE) - header->freeListCount;

    // create new extentListEntry
    // order does not matter, create entry at end of list
    ExtentListEntry * newEntry = extentListPointer + header->extentCount;
    newEntry->segmentID = id;
    newEntry->startPage = entry->startPage;
    newEntry->length = usedLength;
    ++header->extentCount;

    // update freeList entry
    if (availableLength == usedLength) {
        // use whole slice of free space
        // delete freeList entry
        // to avoid "holes in the list", all predecessors have to be moved
        for (int i = (entry - freeListPointer); i >0; --i) {
            // for every predecessor
            freeListPointer[i] = freeListPointer[i-1];
        }
        //update header
        --header->freeListCount;
        header->freeSpace = header->freeSpace - usedLength;
    } else {
        // update freeList entry as it is a smaller slice now: startPage of FreeList entry is moved by 'usedLength' pages
	// length is reduced by 'usedLength' pages.
        entry->startPage = entry->startPage + usedLength;
        entry->length = entry->length - usedLength;
        header->freeSpace = header->freeSpace -  usedLength;
    }
}

void SegmentInventory::deleteExtent(ExtentListEntry * entry) {
    // given entry will be deleted, therefore add the pages into freeList
    unsigned int intervalStart = entry->startPage;
    unsigned int intervalEnd = entry->startPage + entry->length;
    //cout << "freelist: interval to insert [" << intervalStart << " " << intervalEnd << "]" << endl;

    // search FreeList in front of and behind newly freed interval
    // subtracts freeListCount entries from the end!
    FreeListEntry * freeListPointer = reinterpret_cast<FreeListEntry *>(static_cast<char *>(bufferFrame.getData()) + PAGESIZE) - header->freeListCount;
    FreeListEntry * predecessor = 0;
    FreeListEntry * successor = 0;
    for (unsigned int i = 0; i < header->freeListCount; ++i) {
        // for each entry in freeList
        FreeListEntry& tmpEntry = freeListPointer[i];
        //cout << "freelist: interval to compare [" << tmpEntry.startPage << " " << (tmpEntry.startPage + tmpEntry.length) << "]" << endl;
        if (tmpEntry.startPage + tmpEntry.length == intervalStart) {
            // predecessor found!
            predecessor = freeListPointer + i;
        } else if (tmpEntry.startPage == intervalEnd) {
            // successor found!
            successor = freeListPointer + i;
        }
	//Abort the search, if we already found a predecessor and a successor
	if (successor != 0 && predecessor != 0) {
	   break;
	}
    }

    // integrate newly freed interval into freeList
    // 4 cases: none found, pred found, succ found, both found
    // when deleting first/last freeListEntry: similar to "pred not found" // "succ not found"
    if (predecessor != 0 && successor != 0) {
        //cout << "freelist: both found" << endl;
        // both found: merging both intervals into one, move all predecessing entries
        successor->startPage = predecessor->startPage;
        successor->length = predecessor->length + entry->length + successor->length;

        // delete predecessor
        // to avoid "holes in the list", all predecessors have to be moved
        // we have to move all predecessors
        for (int i = (predecessor - freeListPointer); i >0; --i) {
            // for every predecessor
            freeListPointer[i] = freeListPointer[i-1];
        }

        //update header
        --header->freeListCount;
    } else if (predecessor != 0) {
        //cout << "freelist: pred found" << endl;
        // grow predecessing interval
        predecessor->length = predecessor->length + entry->length;
    } else if (successor != 0) {
        //cout << "freelist: succ found" << endl;
        // grow successing interval
        successor->startPage = intervalStart;
        successor->length = successor->length + entry->length;
    } else {
        // none found: insert interval, move all predecessing entries
        //cout << "freelist: none found" << endl;

        // find place to insert new entry
        FreeListEntry * predecessor = 0; // last element we have to move
	if (header->freeListCount == 1) {
            if (entry->startPage >= freeListPointer->startPage) {
                // we have to move entries
                predecessor = freeListPointer;
            }
        } else {
            for (unsigned int i = 1; i < header->freeListCount; ++i) {
                // for each entry in freeList
                FreeListEntry * before = freeListPointer + (i - 1);
                FreeListEntry * after = freeListPointer + i;
                if (before->startPage + before->length < entry->startPage && after->startPage > entry->startPage) {
                    // found right place to insert interval
                    predecessor = before;
                    break;
                }
            }
            // if predecessor is not set, we have to insert in front
        }

        FreeListEntry * placeToInsert = 0;
        if (predecessor == 0) {
            // insert in front
            placeToInsert = freeListPointer - 1;
        } else {
            // move entries
            for (int i = 0; i <= (predecessor - freeListPointer); ++i) {
                // for every predecessor
                freeListPointer[i-1] = freeListPointer[i];
            }
            placeToInsert = predecessor;
        }

        // create and insert new entry
        placeToInsert->startPage = entry->startPage;
        placeToInsert->length = entry->length;
        
        //update header
        ++header->freeListCount;
    }
    //update header
    header->freeSpace = header->freeSpace + entry->length;

    //cout << "-----" << endl;
}

unique_ptr<RegularSegment> SegmentInventory::createSegmentWithID(unsigned int min, unsigned int max, int id) {
    // allocate extent
    allocate(min, max, id);
    
    return retrieveSegment(id);
}
