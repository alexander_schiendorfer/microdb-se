#include "TreeStructs.h"
#include "../segments/Structs.h"
#include "../buffer/BufferManager.h"
#include "../segments/RegularSegment.h"
#include <list>
#include <iostream>
#include <list>
#include <sstream>
#include "RangeIterator.cpp"
#include "Node.cpp"
#include "../util/tid.h"

using namespace std;

template <class K, class Compare = std::less<K>>
class B_Tree {
  private:

    BufferManager& bm;
    RegularSegment& rseg;
    unsigned int rootPage;
    unsigned int nextFreePage = 0;
    Compare keyCompare; // to support key function objects
    bool (*keyCmpFunc)(const K&, const K&); // to support key compare functions
    /**
     * Returns the next free page in the used segment
     */
    unsigned int getNextFreePage() {
        return rseg[nextFreePage++];
    }

    /**
     * Unfixes the page of the given node and frees the memory of the node
     */
    void privateUnfixPage(Node<K>* node, bool isDirty) {
    	bm.unfixPage(node->getBufferFrame(), isDirty);
    	delete node;
    }

    /**
     * Does the recursive lookup logic
     * excl defines whether the returned node is exclusively locked or not
     */
    Node<K>* privateLookup(K key, unsigned int currentPageId, bool excl) {
    	Node<K>* current = new Node<K>(bm.fixPage(currentPageId, excl));
        if (current->isLeaf()) {
            return current;
        } else {
            unsigned int nextPage = current->findPageIdValue(key);
            if (nextPage == 0) {
            	cout << "Could not find a page for key " << key << endl;
            	return NULL;
            }
            privateUnfixPage(current, false);
            
            return privateLookup(key, nextPage, excl);
        }
    }
    
    /**
     * Splits the given node and returns the newly created sibling
     * The lockedParent is given, too. We need it to fix the outgoing links
     */
    Node<K>* splitNode(Node<K>* lockedParent, Node<K>* nodeToSplit, Node<K>* newNode) {
        //Copy half of the keys and values to the new node
        newNode = nodeToSplit->splitNode(newNode);

        //There is no parent node, because we 're splitting the root
        Node<K>* newRoot = NULL;
        if (lockedParent == NULL) {
        	newRoot = createNewRoot(nodeToSplit->getMaxKey(), nodeToSplit->getBufferFrame().getPageNo(), newNode->getBufferFrame().getPageNo());
        } else {
        	unsigned int pageNo = nodeToSplit->getBufferFrame().getPageNo();
        	lockedParent->insert(nodeToSplit->getMaxKey(), &pageNo);
        }

        return newRoot;
    }

    /**
     * Creates a new root and fills in the key, value pair and sets the upper pointer
     */
    Node<K>* createNewRoot(K key, unsigned int val, unsigned int upper) {
    	//Get the res for the new root
		unsigned int newRootPageId = getNextFreePage();
		BufferFrame& bf = bm.fixPage(newRootPageId, true);
		Node<K>* newRoot = new Node<K>(bf);
		rootPage = newRootPageId;
		newRoot->setLeaf(false);
		newRoot->insert(key, &val);
		newRoot->setUpper(upper);

		return newRoot;
    }

    /**
     * Performes the recursive insert logic using coupled locking
     */
    void privateInsert(Node<K>* lockedParent, unsigned int targetNodeId, K key, void* val) {
		//Do we have to create a new root node?
    	if (targetNodeId == 0) {
    		cout << "An error occurred in the private insert method" << endl;
    		return;
    	}
        BufferFrame& bf = bm.fixPage(targetNodeId, true);
        Node<K>* node = new Node<K>(bf);

        //Is enough space in the current node?
        if (node->isSpaceFree()) {
        	//Is the current node a leaf?
        	if (node->isLeaf()) {
        		//We found the correct leaf, insert and finish
        		node->insert(key, val);
        		//Unlock the parent node - NULL if current node is root
        		if (lockedParent != NULL) {
        			privateUnfixPage(lockedParent, false);
        		}
        		//Unlock the leaf, but set dirty
        		privateUnfixPage(node, true);
        	} else {
        		//Find the next child to go into
        		unsigned int nextChildId = node->findPageIdValue(key);
        		//Unlock the parent node - NULL if current node is root
        		if (lockedParent != NULL) {
        			privateUnfixPage(lockedParent, false);
        		}
        		//Call the insert method recursively
        		privateInsert(node, nextChildId, key, val);
        	}
        //Not enough space, so we have to split
        } else {
            //Get a page for the new node
            BufferFrame& bf0 = bm.fixPage(getNextFreePage(), true);
            //Create the new node
            Node<K>* newNode = new Node<K>(bf0);

        	Node<K>* newRoot = splitNode(lockedParent, node, newNode);
        	if (newRoot != NULL) {
        		lockedParent = newRoot;
        	//No new root was created
        	} else {
        		lockedParent->updatePageIdForInsertKey(key, bf0.getPageNo());
        	}
        	//Decide which newly created child to insert into
			//TODO - compare
        	if (node->getMaxKey() < key) {
        		privateUnfixPage(node, true);
        		privateUnfixPage(newNode, true);
        		privateInsert(lockedParent, newNode->getBufferFrame().getPageNo(), key, val);
        	} else {
        		privateUnfixPage(node, true);
        		privateUnfixPage(newNode, true);
        		privateInsert(lockedParent, node->getBufferFrame().getPageNo(), key, val);
        	}
        }
    }

    /**
     * Visualizes the outgoing connections from the node with pageId and calls
     * the method recursively on its children
     */
    string privateVisualizeConnections(unsigned int pageId) {
    	if (pageId == 0) {
    		return "";
    	}
    	BufferFrame& bf = bm.fixPage(pageId, false);
    	Node<K>* node = new Node<K>(bf);
        ostringstream oss;
        if (!node->isLeaf()) {
            for (unsigned int i = 1; i <= node->getEntryCount(); ++i) {
            	oss << "node" << bf.getPageNo() << ":val" << i << " -> node" << node->getValueAt(i-1) << ":pageId;" << endl;
            	oss << privateVisualizeConnections(node->getValueAt(i-1)) << endl;
    		}
            if (node->getUpper() != 0) {
            	oss << "node" << bf.getPageNo() << ":next -> " << "node" << node->getUpper() << ":pageId;" << endl;
            	oss << privateVisualizeConnections(node->getUpper());
            }
        } else {
        	if (node->getNextSibling() != 0) {
        		oss << "node" << bf.getPageNo() << ":next -> " << "node" << node->getNextSibling() << ":pageId;";
        	}
        }
        privateUnfixPage(node, false);

        return oss.str();
    }

    /**
     * Visualizes the node with pageId and calls
     * the method recursively on its children
     */
    string privateVisualizeNodes(unsigned int pageId) {
    	if (pageId == 0) {
    		return "";
    	}
    	BufferFrame& bf = bm.fixPage(pageId, false);
    	Node<K>* node = new Node<K>(bf);
    	string tmp = node->print();
    	for (unsigned int i = 1; i <= node->getEntryCount(); ++i) {
			if (!node->isLeaf()) {
				tmp += privateVisualizeNodes(node->getValueAt(i-1)) + "\n";
			}
		}
    	if (!node->isLeaf()) {
    		if (node->getUpper() != 0) {
    			tmp += privateVisualizeNodes(node->getUpper()) + "\n";
    		}
    	}
    	privateUnfixPage(node, false);

    	return tmp;
    }

  public: 
    /**
     * Uses a segment as housing and creates an empty root
     */
    B_Tree(BufferManager& bm, RegularSegment& seg) : bm(bm), rseg(seg) {

        cout << "Constructor called" << endl;

        //The first page of the segment contains the root
        rootPage = getNextFreePage();
        //Make root leaf
        BufferFrame& bf = bm.fixPage(rootPage, true);
        Node<K>* root = new Node<K>(bf);
        root->setLeaf(true);

        privateUnfixPage(root, true);
    }

    ~B_Tree() {
        cout << "Destructor called" << endl;
    }

    /**
     * Inserts a key/TID pair into the tree
     * encapsulates the privateInsert method, calling it for the root
     */
    void insert(K key, TID val) {
        privateInsert(NULL, rootPage, key, &val);
    }

    /**
     * Deletes the specified key and its value
     */
    void remove(K key) {
        Node<K>* node = privateLookup(key, rootPage, true);
        if (node == NULL) {
			cout << "Could not find a page to remove the value of key " << key << endl;
			return;
		}
        cout << "Trying to remove: " << key << endl;
        cout << "Old capacity: " << node->getFreeSpace() << endl;
        node->remove(key);
        cout << "New capacity: " << node->getFreeSpace() << endl;
        privateUnfixPage(node, true);
    }

    /**
     * Lookup returns a TID or indicates that the key was not found.
     * Encapuslates the privateLookup method
     */
    TID* lookup(K key) {
        Node<K>* node = privateLookup(key, rootPage, false);
        if (node == NULL) {
        	cout << "There is no leaf containing a value for key:" << key << endl;
        	return NULL;
        }
        TID* value = node->findTidValue(key);
        if (value == NULL) {
            cout << "There is no value for key:" << key << endl;
            privateUnfixPage(node, false);
            return NULL;
        } else {
            cout << "Found TID for key:" << key << endl;
            privateUnfixPage(node, false);
            return static_cast<TID*>(value);
        }
    }

    /**
     * Returns an interator that allows to iterate over the result set
     */
    RangeIterator<K>* lookupRange(K start, K end) {
    	Node<K>* node = privateLookup(start, rootPage, false);
    	return new RangeIterator<K>(bm, node, start, end);
    }
    

    /**
     * Outputs the tree structure using Graphviz/dot code
     */
    string visualize() {
    	ostringstream oss;
        oss << "digraph B⁺-Tree {" << endl;

        oss << privateVisualizeNodes(rootPage);

        oss << privateVisualizeConnections(rootPage);

        oss << "}" << endl;

        return oss.str();
    }
};
