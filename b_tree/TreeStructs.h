#ifndef TREE_STRUCTS_H
#define TREE_STRUCTS_H

struct NodeData {
    unsigned int LSN; //Log sequence number
    bool isLeaf; //Indicates whether it is a leaf or not
    unsigned int entryCount; //Number of key/pageNo pairs
    unsigned int rightMostEntry; //ID of the right most entry in the node (i.e. upper or next sibling)
};

#endif
