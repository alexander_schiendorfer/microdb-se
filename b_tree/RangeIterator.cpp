#ifndef DBS_RANGEIT_CPP
#define DBS_RANGEIT_CPP

#include <iostream>
#include "Node.cpp"
#include "../buffer/BufferManager.h"
#include <stdexcept>

using namespace std;

template <class K>
class RangeIterator {
private:
	K startKey, endKey;
	Node<K>* curr;
	K* keyOffset;
	BufferManager& bm;

    /**
     * Unfixes the page of the given node and frees the memory of the node
     */
    void privateUnfixPage(Node<K>* node, bool isDirty) {
    	bm.unfixPage(node->getBufferFrame(), isDirty);
    	delete node;
    }

public:
    /**
     * Constructor for the RangeIterator
     * Ranging from startKey to endKey
     * Starting at startNode
     */
	RangeIterator(BufferManager& bm, Node<K>* startNode, K startKey, K endKey) : bm(bm) {
		this->startKey = startKey;
		this->endKey = endKey;
		this->curr = startNode;
		this->keyOffset = startNode->getKeyOffsetForKey(startKey);
	}

	//TID* operator*() {
	/**
	 * Returns the value at the current position of the iterator
	 */
	TID* get() {
		if (curr == NULL) {
			return NULL;
		}
		return curr->findTidValue(*keyOffset);
	}

	/**
	 * Returns the keyOffset of the iterator
	 */
	K* getKeyOffset() {
		return keyOffset;
	}

	//RangeIterator& operator++(int){
	/**
	 * Increments the position of the iterator
	 */
	RangeIterator& inc() {
		//We reached the end
		if (*keyOffset == endKey) {
			privateUnfixPage(curr, false);
			curr = NULL;
			return (*this);
		}
		if (keyOffset == (curr->getKeyOffset() + curr->getEntryCount() - 1)) {
			if (curr->getNextSibling() != 0) {
				BufferFrame& bf = bm.fixPage(curr->getNextSibling(), false);
				privateUnfixPage(curr, false);
				curr = new Node<K>(bf);
				keyOffset = curr->getKeyOffset();
			}
		} else {
			keyOffset++;
		}
		return (*this);
	}

	/**
	 * Implements the == operator for the iterator
	 */
	bool operator==(RangeIterator& b) const {
		return *(b.getKeyOffset()) == *(this->keyOffset);
	}

	/**
	 * Implements the != operator for the iterator
	 */
	bool operator!=(RangeIterator& b) const {
		return *(b.getKeyOffset()) != *(this->keyOffset);
	}

};
#endif
