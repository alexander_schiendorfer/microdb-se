#include "PlanExecuter.h"
#include "Plan.h"
#include "Operator.h"
#include "TableScanOperator.h"
#include "ProjectionOperator.h"
#include "SelectionOperator.h"
#include "../schema/Schema.h"
#include "../schema/Metadata.h"
#include <memory>

PlanExecuter::PlanExecuter(BufferManager& bufferManager, SegmentInventory& segmentInventory, Metadata& metadata) : bm(bufferManager), si(segmentInventory), md(metadata) {
  // empty
}

PrintOperator PlanExecuter::execute(const char* fileName) {
  plan::Plan p;
  p.fromFile(fileName);
  const plan::Operator* root = &p.getRoot();
  Operator* op = getOperatorHierarchy(root);

  PrintOperator pr(op, std::cout);
  return pr;
}

Operator* PlanExecuter::getOperatorHierarchy(const plan::Operator* op) {
  switch (op->getOperatorType()) {
    case plan::OperatorType::TableScan: {
      // leaf
      std::string name = reinterpret_cast<plan::TableScan*>(const_cast<plan::Operator*>(op))->name;
      unsigned int relId = md.lookup(name);
      unique_ptr<Schema::Relation> rel = std::move(md.getRelation(relId));
      //unique_ptr<RelationSegment> seg = std::move(si.retrieveRelationSegment(relId, rel.get()));
      unique_ptr<RelationSegment> seg = std::move(si.getRelationSegment(relId, rel.get(), false)); //TODO da bin ich

      TableScanOperator* ts = new TableScanOperator(std::move(rel), std::move(seg), &bm);

      return ts;
    }
    case plan::OperatorType::MergeJoin: {
      //TODO
    }
    case plan::OperatorType::Select: {
      vector<unsigned> unsignedAttributes = reinterpret_cast<plan::Select*>(const_cast<plan::Operator*>(op))->attributeIds;
      vector<int> attributes;
      for (auto it = unsignedAttributes.begin(); it != unsignedAttributes.end(); ++it) {
        attributes.push_back(static_cast<int>(*it)); // todo right values?
      }

      vector<std::string> stringConstants = reinterpret_cast<plan::Select*>(const_cast<plan::Operator*>(op))->constants;
      vector<Register*> constants;
      for (auto it = stringConstants.begin(); it != stringConstants.end(); ++it) {
        Register* r = new Register();
        r->setString(*it);
        constants.push_back(r);
      }

      SelectionOperator* s = new SelectionOperator(getOperatorHierarchy(&(reinterpret_cast<plan::UnaryOperator*>(const_cast<plan::Operator*>(op))->getChild())), attributes, constants);
      return s;
    }
    case plan::OperatorType::Project: {
      vector<unsigned> unsignedAttributes = reinterpret_cast<plan::Select*>(const_cast<plan::Operator*>(op))->attributeIds;
      vector<int> attributes;
      for (auto it = unsignedAttributes.begin(); it != unsignedAttributes.end(); ++it) {
        attributes.push_back(static_cast<int>(*it)); // todo right values?
      }

      ProjectionOperator* p = new ProjectionOperator(getOperatorHierarchy(&(reinterpret_cast<plan::UnaryOperator*>(const_cast<plan::Operator*>(op))->getChild())), attributes);
      return p;
    }
    case plan::OperatorType::Sort: {
      //TODO
    }
    default: throw;
  }
}
