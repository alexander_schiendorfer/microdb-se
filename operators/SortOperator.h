/*
 * SortOperator.h
 *
 *  Created on: Jul 2, 2012
 *      Author: alexander
 */

#ifndef SORTOPERATOR_H_
#define SORTOPERATOR_H_

#include "Operator.h"
#include <vector>
/**
 * Just performs a sort in main memory
 * for simulating a sort/merge join
 */
class SortOperator: public Operator {
private:
	Operator* input;
	std::vector<std::vector<Register*> > buffer;
	bool ascending;
	const std::vector<unsigned>& attributes;
	int currentTuple;

public:
	SortOperator(Operator* input, bool ascending, const std::vector<unsigned>& attributes);
	virtual ~SortOperator();
	// when opened, relation is read and stored in buffer
	void open();
	// Produce the next tuple
	bool next();
	// Get all produced values
	std::vector<Register*> getOutput();
	// Close the operator
	void close();
};

#endif /* SORTOPERATOR_H_ */
