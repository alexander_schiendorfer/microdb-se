/*
 * PrintOperator.h
 *
 *  Created on: Jun 10, 2012
 *      Author: alexander
 */

#ifndef PRINTOPERATOR_H_
#define PRINTOPERATOR_H_

#include "Operator.h"
#include <iostream>

class PrintOperator: public Operator {
private:
	Operator* input;
	std::ostream& output;

public:
	PrintOperator(Operator* input, std::ostream& output);
	virtual ~PrintOperator();
	void open();
	// Produce the next tuple
	bool next();
	// Get all produced values
	std::vector<Register*> getOutput();
	// Close the operator
	void close();
};

#endif /* PRINTOPERATOR_H_ */
