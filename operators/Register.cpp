/*
 * Register.cpp
 *
 *  Created on: Jun 10, 2012
 *      Author: alexander
 */

#include "Register.h"

Register::Register() {
	// TODO Auto-generated constructor stub

}

Register::~Register() {
	// TODO Auto-generated destructor stub
}


bool Register::isInt() const {
	return isInteger;
}

void Register::setInt(bool isIntval) {
	this->isInteger = isIntval;
}

int Register::getInteger() const {
	return intValue;
}

void Register::setInteger(int intValue) {
	this->intValue = intValue;
}

std::string Register::getString() const {
	return strValue;
}

void Register::setString(std::string strValue) {
	this->strValue = strValue;
}

bool Register::equals(Register* other) {
	if(isInteger != other->isInteger) {
		std::cerr << "WARNING: comparing invalid types!!" << std::endl;
		return false;
	} else {
		if(isInteger)
			return this->intValue == other->intValue;
		else
			return this->strValue.compare(other->strValue) == 0;
	}
}

int Register::compare(Register* other) {
	if(isInteger != other->isInteger) {
		std::cerr << "WARNING: comparing invalid types!!" << std::endl;
		return 0;
	} else {
		if (isInteger) {
			if (intValue == other->intValue) {
				return 0;
			} else if (intValue > other->intValue) {
				return 1;
			} else {
				return -1;
			}
		} else {
			return this->strValue.compare(other->strValue);
		}
	}
}

unsigned Register::getLength() const {
	if(isInteger) return sizeof(int);

	return length;
}

void Register::setLength(unsigned length) {
	this->length = length;
}
