/*
 * SelectionOperator.cpp
 *
 *  Created on: Jun 28, 2012
 *      Author: alexander
 */

#include "SelectionOperator.h"
SelectionOperator::SelectionOperator(Operator* input,const std::vector<int>& attributes, const std::vector<Register*>& constants ) : input(input), attributes(attributes), constants(constants) {

}

SelectionOperator::~SelectionOperator() {
	// TODO Auto-generated destructor stub
}

void SelectionOperator::close(){
	input->close();
}

void SelectionOperator::open() {
	input->open();
}

// Produce the next tuple
bool SelectionOperator::next() {
	while(input->next()) {
		std::vector<Register*> registers = input->getOutput();

		// iterate through attributes, take them as index
		int outputIndex = 0;
		for(auto it = attributes.begin(); it != attributes.end(); it++, outputIndex++) {
			Register* constant = constants[outputIndex];
			Register* valueInTuple = registers[*it];
			if(valueInTuple->equals(constant)) {
				output.clear();
				for(auto regIt = registers.begin(); regIt != registers.end(); regIt++) {
					output.push_back(*regIt);
				}
				return true;
			} else
				continue;
		}
	}
	return false;
}

// Get all produced values
std::vector<Register*> SelectionOperator::getOutput() {
	return output;
}

