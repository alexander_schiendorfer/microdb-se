/*
 * Operator.h
 *
 *  Created on: Jun 10, 2012
 *      Author: alexander
 */

#ifndef OPERATOR_H_
#define OPERATOR_H_

#include <vector>
#include "Register.h"

/**
 * Abstract base class for set oriented
 * algebraic operators
 *
 * Conventions:
 *
 * open does not write the first tuple into output
 * hence next has to be called for every tuple
 *
 * close needs to be called explicitly, not by destructor
 */
class Operator {
protected:
	std::vector<Register*> output;

public:
	Operator();
	virtual ~Operator();
	virtual void open() = 0;
	// Produce the next tuple
	virtual bool next() = 0;
	// Get all produced values
	virtual std::vector<Register*> getOutput() = 0;
	// Close the operator
	virtual void close() = 0;
};

#endif /* OPERATOR_H_ */
