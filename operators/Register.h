/*
 * Register.h
 *
 *  Created on: Jun 10, 2012
 *      Author: alexander
 */

#ifndef REGISTER_H_
#define REGISTER_H_
#include <string>
#include <iostream>

class Register {
private:
	int intValue;
	std::string strValue;
	bool isInteger;
	unsigned length; // needed for sort operator; contains maximal length of string
	// or sizeof(int)

public:
	Register();
	virtual ~Register();

	bool isInt() const;
	void setInt(bool isIntval) ;
	int getInteger() const ;
	void setInteger(int intValue);
	std::string getString() const ;
	void setString(std::string strValue);
	bool equals(Register* other);
	int compare(Register* other);
	unsigned getLength() const;
	void setLength(unsigned length);
};

#endif /* REGISTER_H_ */
