/*
 * TableScanOperator.cpp
 *
 *  Created on: Jun 11, 2012
 *      Author: alexander
 */

#include "TableScanOperator.h"
#include "../buffer/BufferFrameHolder.h"
#include "../buffer/BufferFrame.h"
#include "../schema/Types.h"
#include <string>

TableScanOperator::TableScanOperator(const Schema::Relation* relation, BufferManager* bufferManager) : relation(relation), bufferManager(bufferManager) {
	// TODO get SPSegment* from meta data
}

TableScanOperator::TableScanOperator(unique_ptr<Schema::Relation> relation, unique_ptr<RelationSegment> segment, BufferManager* bufferManager) : relationUniquePtr(std::move(relation)), segmentUniquePtr(std::move(segment)), bufferManager(bufferManager){
	this->relation = relationUniquePtr.get();
	this->segment = segmentUniquePtr.get();
}

TableScanOperator::TableScanOperator(const std::string& name, BufferManager* bufferManager) : bufferManager(bufferManager) {
	// TODO get relation and spsegment from meta data
}

TableScanOperator::TableScanOperator(const Schema::Relation* relation, const SPSegment* segment, BufferManager* bufferManager) :
		relation(relation), segment(segment), bufferManager(bufferManager) {

}

TableScanOperator::~TableScanOperator() {
	// TODO into superclass
	for(unsigned int i = 0; i < output.size(); i++)
		delete output[i];
}

void TableScanOperator::open() {
	segmentPageIndex = 0;
	currentPage = (*segment)[0];
	tupleOnPage = 0;

	// initialize register vector according to attributes
	for(auto attrIt = relation->attributes.begin(); attrIt != relation->attributes.end(); attrIt++){
		const Schema::Relation::Attribute& attr = *attrIt;
		Register* reg = new Register();

		// TODO distinguish types and write them into relation
		if(attr.type == Types::Tag::Integer) {
			reg->setInt(true);
			reg->setLength(sizeof(int));
		} else {
			reg->setInt(false);
			reg->setLength(attr.len);
		}

		output.push_back(reg);
	}
}

// Produce the next tuple
bool TableScanOperator::next() {
	while(segmentPageIndex < segment->getSize()) {
		BufferFrameHolder holder(*bufferManager, currentPage, true);
		BufferFrame& frame = holder.getProtectedFrame();
		holder.setDirty(false);
		const SPHeader& header =  *static_cast<SPHeader*>(frame.getData());
	//	std::cout << "accessing page " << frame.getPageNo() << " from relation " << relation->name << std::endl;
		while(tupleOnPage < header.slotCount) {
			const Slot& slot = header.firstSlot[tupleOnPage];
			if(slot.isReference) {
				// TODO implement access on referenced page
			} else {
				if(!slot.unused) {
					char* recData = static_cast<char*>(frame.getData()) + slot.offset;
					copyToRegister(recData, slot.length);
					tupleOnPage++;
					return true;
				}
			}
			tupleOnPage++;
		}
		segmentPageIndex++;
	}
	return false;
}

// Get all produced values
std::vector<Register*> TableScanOperator::getOutput() {
	return output;
}

void TableScanOperator::copyToRegister(char* data, unsigned length) {
	// iterate through relation
	char* current = data;
	int regId = 0;
	for(auto attrIt = relation->attributes.begin(); attrIt != relation->attributes.end(); attrIt++){
		const Schema::Relation::Attribute& attr = *attrIt;
		Register* reg = output[regId];

		// TODO distinguish types and write them into relation
		if(attr.type == Types::Tag::Integer) {
			int* castPtr = reinterpret_cast<int*>(current);
			reg->setInteger(*castPtr);

			current += sizeof(int);
		} else {

			std::string newString(current, attr.len);
			reg->setString(newString);
			current += attr.len;
		}
		regId++;
	}
}

// Close the operator
void TableScanOperator::close() {
	// TODO use unique_ptr for ownership and create extra vector

}

