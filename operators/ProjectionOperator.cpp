/*
 * ProjectionOperator.cpp
 *
 *  Created on: Jun 28, 2012
 *      Author: alexander
 */

#include "ProjectionOperator.h"

ProjectionOperator::ProjectionOperator(Operator* input, const std::vector<int>& attributes) : input(input), attributes(attributes) {
	// create register vector
	Register* newReg = NULL;
	for(unsigned int i = 0; i < attributes.size(); i++) {
		output.push_back(newReg);
	}
}

ProjectionOperator::~ProjectionOperator() {

}

void ProjectionOperator::close(){
	input->close();
}

void ProjectionOperator::open() {
	input->open();
}

// Produce the next tuple
bool ProjectionOperator::next() {
	if(!input->next())
		return false;
	std::vector<Register*> registers = input->getOutput();

	// iterate through attributes, take them as index
	int outputIndex = 0;
	for(auto it = attributes.begin(); it != attributes.end(); it++, outputIndex++) {
		int pos = *it;
		output[outputIndex] = registers[pos];
	}
	return true;
}

// Get all produced values
std::vector<Register*> ProjectionOperator::getOutput() {
	return output;
}
