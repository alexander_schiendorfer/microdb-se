/*
 * ExternalSortOperator.h
 *
 *  Created on: Jun 29, 2012
 *      Author: alexander
 */

#ifndef EXTERNALSORTOPERATOR_H_
#define EXTERNALSORTOPERATOR_H_

#include "Operator.h"

// partition info helper class
// contains information about pointer in buffer
// and how long to read
class partition_info {
  public :

  int fileDescr;
  char* currentPos;
  char* endPos;
  unsigned long lastNumber;
  unsigned long readBytes;

  bool isEmpty() {
    return currentPos >= endPos;
  }

  bool isComplete() {
    return this->readBytes <= 0;
  }

  void setReadBytes(unsigned long  readBytes) {
    this->readBytes = readBytes;
    endPos = currentPos + readBytes;
  }

  void readNext() {
     this->lastNumber = *((unsigned long *) this->currentPos);
     this->currentPos += sizeof(uint64_t);
  }


};

/**
 * Performs an external sort based on an input operator and
 * external file sorting
 * assumes that available memory is large enough to have k tuples in
 * main memory where k is the number of files needed to process
 * input hence k = InputSize / Available Memory
 */
class ExternalSortOperator: public Operator {
private:
	Operator* input;
	std::ostream& output;
	unsigned tupleSize;

	// returns the file name for file i
	inline void getFileName(int i, char* sbuffer) {
	   sprintf (sbuffer, "tmp/%d.tmp", i);
	} // getFileName

	void readFromFile(char* buffer, int i, partition_info tempParts[], unsigned long partBytes);
	void externalSort(int fdInput,
	                  unsigned long size,
	                  int fdOutput,
	                  unsigned long memSize);
public:
	ExternalSortOperator();
	virtual ~ExternalSortOperator();

	/**
	 * Does most of the external sort work thus
	 * reading the possible number of tuples into main memory,
	 * performing quick sort and sending the file back
	 */
	void open();

	/**
	 * Returns the next tuple of priority queue in main memory
	 * (if any) -> picks the next tuple from the file where
	 * head was taken from
	 */
	bool next();

	// Get all produced values
	std::vector<Register*> getOutput();

	// Close the operator
	void close();

};

#endif /* EXTERNALSORTOPERATOR_H_ */
