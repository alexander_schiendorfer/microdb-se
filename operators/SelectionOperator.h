/*
 * SelectionOperator.h
 *
 *  Created on: Jun 28, 2012
 *      Author: alexander
 */

#ifndef SELECTIONOPERATOR_H_
#define SELECTIONOPERATOR_H_

#include "Operator.h"

/**
 * Selection operator performs test for
 * equality (other comparators could be supplied)
 * constants are stored in register objects
 * as they are available for all supported types
 * (and a rudimentary runtime type check is possible :>)
 */

class SelectionOperator: public Operator {
private:
	Operator* input;
	const std::vector<int>& attributes;
	const std::vector<Register*>& constants;

public:
	SelectionOperator(Operator* input,const std::vector<int>& attributes, const std::vector<Register*>& constants );
	virtual ~SelectionOperator();

	void open();
	// Produce the next tuple by selecting tuples as specified in vector
	bool next();
	// Get all produced values
	std::vector<Register*> getOutput();
	// Close the operator
	void close();
};

#endif /* SELECTIONOPERATOR_H_ */
