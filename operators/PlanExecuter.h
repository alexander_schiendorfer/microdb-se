#ifndef PLANEXECUTER_H
#define PLANEXECUTER_H

#include "../buffer/BufferManager.h"
#include "../segments/SegmentInventory.h"
#include "../operators/Operator.h"
#include "../operators/PrintOperator.h"
#include "../operators/Plan.h"
#include "../schema/Metadata.h"

/**
 Reads execution plans from file, parses them using the provided parser, executes them and finally prints the results. 
*/
class PlanExecuter {

  private:
  BufferManager& bm;
  SegmentInventory& si;
  Metadata& md;

  /**
   Private Helper method for building the hierarchy.
   TODO use non-abstract base class "Operator" instead of void-pointers
  */
  Operator* getOperatorHierarchy(const plan::Operator* op); 
  
  public:
  PlanExecuter(BufferManager& bm, SegmentInventory& si, Metadata& md);
  /**
   Reads, parses, executes.
  */
  PrintOperator execute(const char* fileName);
};
#endif
