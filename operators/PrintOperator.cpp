/*
 * PrintOperator.cpp
 *
 *  Created on: Jun 10, 2012
 *      Author: Alexander Schiendorfer
 */

#include "PrintOperator.h"

PrintOperator::PrintOperator(Operator* input, std::ostream& output) : input(input), output(output) {

}

PrintOperator::~PrintOperator() {
	// TODO Auto-generated destructor stub
}

void PrintOperator::close(){
	input->close();
}

void PrintOperator::open() {
	input->open();
}

// Produce the next tuple
bool PrintOperator::next() {
	if(!input->next())
		return false;
	std::vector<Register*> registers = input->getOutput();
	output << "| ";
	for(auto it = registers.begin(); it != registers.end(); it++) {
		auto reg = *it;
		if(reg->isInt()) {
			output << reg->getInteger();
		} else {
			output << reg->getString();
		}
		output << " | ";
	}
	output << std::endl;
	return true;
}

// Get all produced values
std::vector<Register*> PrintOperator::getOutput() {
	return input->getOutput();
}
