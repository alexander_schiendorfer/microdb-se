/*
 * MergeOperator.cpp
 *
 *  Created on: Jun 30, 2012
 *      Author: chris
 */

#include "MergeOperator.h"

using namespace std;

MergeOperator::MergeOperator(Operator* leftInput, Operator* rightInput, const vector<pair<unsigned int, unsigned int>>& joinAttributes) : leftInput(leftInput), rightInput(rightInput), joinAttributes(joinAttributes) {

}

MergeOperator::~MergeOperator() {
	//Auto-generated destructor stub
}

void MergeOperator::close(){
	leftInput->close();
	rightInput->close();
}

void MergeOperator::open() {
	leftInput->open();
	rightInput->open();
}

int MergeOperator::compareLeftWith(vector<Register*> other) {
	for(auto it = joinAttributes.begin(); it != joinAttributes.end(); it++) {
		Register* valueInLeft = leftRegister[(*it).first];
		Register* valueInRight = other[(*it).second];
		int cmpResult = valueInLeft->compare(valueInRight);
		if (cmpResult != 0) {
			return cmpResult;
		}
	}

	return 0;
}

void MergeOperator::prepareOutput() {
	output.clear();
	for(auto it = leftRegister.begin(); it != leftRegister.end(); it++) {
		output.push_back(*it);
	}
	for(auto it = (*bufferIterator).begin(); it != (*bufferIterator).end(); it++) {
		output.push_back(*it);
	}
}

// Produce the next tuple
bool MergeOperator::next() {
	while(true) {
		//Wait for the left operator to produce a tuple
		if (leftRegister.empty()) {
			if (!leftInput->next())
				return false;
			leftRegister = leftInput->getOutput();
		}

		//Are there elements in the buffer to compare with
		if (!buffer.empty()) {
			if(compareLeftWith(*bufferIterator) == 0) {
				prepareOutput();

				bufferIterator++;
				if (bufferIterator == buffer.end()) {
					bufferIterator = buffer.begin();
					leftRegister.clear();
				}
				return true;
			} else {
				buffer.clear();
			}
		} else {
			//Get the next tuple from the right operator
			if (rightRegister.empty()) {
				if(!rightInput->next())
					return false;
				rightRegister = rightInput->getOutput();
			}

			//If the left is greater than the right, get the next right tuple
			while(compareLeftWith(rightRegister) == 1) {
				if (!rightInput->next())
					return false;
				rightRegister = rightInput->getOutput();
			}

			//If the left is equal to the right, buffer the right tuples
			//until we reach a not equal tuple
			while(compareLeftWith(rightRegister) == 0) {
				//Buffer the matches
				buffer.push_back(getTempClone(rightRegister));
				if (!rightInput->next())
					break;
				rightRegister = rightInput->getOutput();
			}

			if (buffer.empty()) {
				leftRegister.clear();
			} else {
				bufferIterator = buffer.begin();
			}
		}
	}
	return false;
}

vector<Register*> MergeOperator::getTempClone(vector<Register*> base) {
	vector<Register*> tmp;
	for(auto it = base.begin(); it != base.end(); it++) {
		Register* reg = new Register();
		Register* baseReg = *it;
		reg->setInt(baseReg->isInt());
		if (baseReg->isInt()) {
			reg->setInteger(baseReg->getInteger());
		} else {
			reg->setString(baseReg->getString());
		}
		tmp.push_back(reg);
	}

	return tmp;
}

// Get all produced values
vector<Register*> MergeOperator::getOutput() {
	return output;
}
