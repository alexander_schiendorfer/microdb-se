/*
 * MergeOperator.h
 *
 *  Created on: Jun 30, 2012
 *      Author: chris
 */

#ifndef MERGEOPERATOR_H_
#define MERGEOPERATOR_H_

#include "Operator.h"

using namespace std;

class MergeOperator: public Operator {
private:
	Operator* leftInput;
	Operator* rightInput;
	vector<Register*> leftRegister;
	vector<Register*> rightRegister;
	const vector<pair<unsigned int, unsigned int>>& joinAttributes;
	vector<vector<Register*>> buffer;
	vector<vector<Register*>>::iterator bufferIterator;
	void prepareOutput();
	int compareLeftWith(vector<Register*> other);
	vector<Register*> getTempClone(vector<Register*>);

public:
	MergeOperator(Operator* leftInput, Operator* rightInput, const vector<pair<unsigned int, unsigned int>>& attributes);
	virtual ~MergeOperator();
	void open();
	// Produce the next tuple
	bool next();
	// Get all produced values
	vector<Register*> getOutput();
	// Close the operator
	void close();
};

#endif /* MERGEOPERATOR_H_ */
