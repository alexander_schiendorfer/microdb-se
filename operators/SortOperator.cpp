/*
 * SortOperator.cpp
 *
 *  Created on: Jul 2, 2012
 *      Author: alexander
 */

#include "SortOperator.h"
#include <algorithm>

SortOperator::SortOperator(Operator* input, bool ascending, const std::vector<unsigned>& attributes)
: input(input), ascending(ascending), attributes(attributes) {
	// TODO Auto-generated constructor stub

}

SortOperator::~SortOperator() {
	for(auto bufferIt = buffer.begin(); bufferIt != buffer.end(); bufferIt++) {
		for(auto regIt = (*bufferIt).begin(); regIt != bufferIt->end(); regIt++) {
			delete *regIt;
		}
	}
}

void SortOperator::close(){
	input->close();
}

void SortOperator::open() {
	input->open();
	while(input->next()) {
		std::vector<Register*> bufferedTuple;

		const std::vector<Register*>& inputOutput = input->getOutput();
		for(auto it = inputOutput.begin(); it != inputOutput.end(); it++) {
			// todo use a proper copy constructor here
			Register* bufferedReg = new Register();
			Register* oldReg = (*it);

			bufferedReg->setInt(oldReg->isInt());
			bufferedReg->setLength(oldReg->getLength());
			if(bufferedReg->isInt()){
				bufferedReg->setInteger(oldReg->getInteger());
			} else {
				bufferedReg->setString(oldReg->getString());
			}
			bufferedTuple.push_back(bufferedReg);
		}
		buffer.push_back(bufferedTuple);
	}

	// sort the vector
	std::sort(buffer.begin(),buffer.end(),[&](const std::vector<Register*> tuple1, const std::vector<Register*> tuple2) {
		for(auto attrIt = attributes.begin(); attrIt != attributes.end(); attrIt++) {
			Register* firstAttr = tuple1[*attrIt];
			Register* secondAttr = tuple2[*attrIt];

			if(!firstAttr->equals(secondAttr)) {
				int compRes = firstAttr->compare(secondAttr) ;
				if(ascending)
					return compRes < 0;
				else
					return compRes > 0;
			}
		}
		return false;
	});
	currentTuple = 0;
}

// Produce the next tuple
bool SortOperator::next() {
	if(currentTuple < buffer.size()) {
		output = buffer[currentTuple];
		currentTuple++;
		return true;
	} else
		return false;
}

// Get all produced values
std::vector<Register*> SortOperator::getOutput() {
	return output;
}
