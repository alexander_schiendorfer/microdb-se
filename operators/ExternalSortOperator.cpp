/*
 * ExternalSortOperator.cpp
 *
 *  Created on: Jun 29, 2012
 *      Author: alexander
 */

#include "ExternalSortOperator.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <algorithm>  // for sort
#include <functional> // for comparator
#include <queue>      // for priority queue
#include <vector>     // for vector in queue
#include <iterator>
#include <fstream>

/**
Reads partBytes bytes into buffer at position i*partBytes
Gets file from file descriptor tempFiles[i]
Sets tempParts to buffer + i * partBytes
*/
void ExternalSortOperator::readFromFile(char* buffer, int i, partition_info tempParts[], unsigned long partBytes) {
  // pointer arithmetics to get to position in buffer
  tempParts[i].currentPos = buffer + (i * partBytes);

  // make sure you don't break at the wrong point
  unsigned long partBytesMultiple8 = (partBytes / sizeof(uint64_t)) * sizeof(uint64_t);

  // read files to that position
  tempParts[i].setReadBytes(read(tempParts[i].fileDescr, tempParts[i].currentPos, partBytesMultiple8));
} // readFromFile

void printBuffer(char* part, unsigned long partBytes) {
  char* end = part + partBytes;

  while(part + sizeof(uint64_t) < end) {
    unsigned long curNo = *((unsigned long *) part);
    std::cout << curNo << " " ;
    part+=sizeof(uint64_t);
  }
  std::cout << std::endl;
} // printBuffer


/**
Performs external merge sort on
a binary file containing an arbitrary
number of integers using memsize buffer
since memSize is given in MB, it is
a multiple of 8 B so we can be sure not
to break between a number

fdInput .. file descriptor of input file
size    .. 64 bit unsigned integer values
memSize .. number of bytes for buffer
fdOutput .. file descriptor of input file
*/
void ExternalSortOperator::externalSort(int fdInput,
                  unsigned long size,
                  int fdOutput,
                  unsigned long memSize) {
   int ret;            // return value
   int fd;             // file descriptor for temporary files
   char strBuffer[50]; // char buffer for temporary files
   unsigned long fileSize = size * sizeof(uint64_t); // file must consist of exactly size binary 64 bit uint
   char* buffer = new char[memSize];
   // calculate the number of runs we will get
   int k = fileSize / memSize;
   unsigned long remainingBytes = fileSize; // making sure not to read too long
   unsigned long readBytes = memSize;

   if(fileSize % memSize != 0) k++;

   // calculate initial runs
   for(int i = 0; i < k; i++) {
     readBytes = remainingBytes < memSize ? remainingBytes : memSize;
     // read one chunk of them and print them on console
     ret = read(fdInput, buffer, readBytes);
     if(ret < 0) {
       std::cerr << "error at reading file " << std::endl;
     }

     remainingBytes -= readBytes;

     // use pointer cast to interpret each item as unsigned long
     unsigned long* castPtr = (unsigned long*) buffer;

     // sort using iterators (readBytes / sizeof(uint64_t) is the number of uint long elements ), pointer arithmetics at its best :)
     std::sort(castPtr, castPtr + ( readBytes / sizeof(uint64_t)));

     // and write it to file
     getFileName(i, strBuffer);
     if ((fd = open(strBuffer, O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR)) < 0) {
	      std::cerr << "cannot open file '" << strBuffer << "': " << strerror(errno) << std::endl;
	      return;
     }

     if (write(fd, castPtr, readBytes) < 0) {
			std::cout << "error writing to " << i << ": " << strerror(errno) << std::endl;
			return;
		 }
		 close(fd);
   } // for i

   // perform k-way merge
   // -----------------------------------------------
   std::priority_queue<unsigned long , std::vector<unsigned long>, std::greater<unsigned long> > heap;
   char* outputBuffer;
   unsigned long partBytes = memSize / (k + 1);

   struct partition_info tempParts[k];

   for (int i = 0; i < k; i++) {
      getFileName(i, strBuffer);
      if ((tempParts[i].fileDescr =  open(strBuffer, O_RDONLY, 0)) < 0) {
	      std::cerr << "cannot open file '" << strBuffer << "': " << strerror(errno) << std::endl;
	      return;
      }
      readFromFile(buffer, i, tempParts, partBytes);
   }
   outputBuffer = buffer + (k * partBytes);

   // now output buffer in this state

   for (int i = 0; i < k; i++) {
     // enter first number into priority queue
     tempParts[i].readNext();
     heap.push(tempParts[i].lastNumber);
   }

   // posix_fallocates reserves fileSize bytes for our file (useful since we know exactly how large it'll be
   if ( (posix_fallocate(fdOutput, 0, fileSize)) < 0) {
		std::cerr << "cannot reserve space for output file : " << strerror(errno) << std::endl;
		return;
	 }

	 while(!heap.empty()) {
	    unsigned long topElement = heap.top();
	    heap.pop();
	    //std::cout << topElement << std::endl;
	    for(int i = 0; i < k; i++) { // find the first buffer having last-element == topElement for next int
	      if(tempParts[i].lastNumber == topElement && !tempParts[i].isComplete() ) {
	        if(tempParts[i].isEmpty()) { // try one reload
	          readFromFile(buffer, i, tempParts, partBytes);
	        } // isEmpty

	        if(!tempParts[i].isComplete()) {
	          tempParts[i].readNext();
	          heap.push(tempParts[i].lastNumber);
	          break; // only one push per loop
	        } // isComplete
	      } // equal to topElement
	    } // for i

	    // now care for output buffer and you might even write it
	    *((unsigned long *) outputBuffer) = topElement;
	    outputBuffer += sizeof(uint64_t);
	    if(outputBuffer + sizeof(uint64_t) > buffer + memSize) { // output buffer full
	      char* helper = outputBuffer;
	      outputBuffer = buffer + (k * partBytes);
	      if (write(fdOutput, outputBuffer, helper-outputBuffer) < 0) {
			    std::cout << "error writing ... " << strerror(errno) << std::endl;
			    return;
		    }
	    }
	 }
	delete[] buffer;
} // externalSort

ExternalSortOperator::ExternalSortOperator() {
	// TODO Auto-generated constructor stub

}

ExternalSortOperator::~ExternalSortOperator() {
	// TODO Auto-generated destructor stub
}


void ExternalSortOperator::close(){
	input->close();
}

void ExternalSortOperator::open() {
	input->open();
	// fetch first tuple to get record size

	tupleSize = 0;
	if(!input->next()){
		std::vector<Register*>& output = input->getOutput();
		for(auto it = output.begin(); it != output.end(); it++) {
			if((*it)->isInt()){
				tupleSize += sizeof(int);
			} else {
				tupleSize += (*it)->getLength();
			}
		}
	}
}

// Produce the next tuple
bool ExternalSortOperator::next() {
	if(!input->next())
		return false;
	std::vector<Register*> registers = input->getOutput();
	output << "| ";
	for(auto it = registers.begin(); it != registers.end(); it++) {
		auto reg = *it;
		if(reg->isInt()) {
			output << reg->getInteger();
		} else {
			output << reg->getString();
		}
		output << " | ";
	}
	output << std::endl;
	return true;
}

// Get all produced values
std::vector<Register*> ExternalSortOperator::getOutput() {
	return input->getOutput();
}
