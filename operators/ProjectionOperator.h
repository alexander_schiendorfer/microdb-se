/*
 * ProjectionOperator.h
 *
 *  Created on: Jun 28, 2012
 *      Author: alexander
 */

#ifndef PROJECTIONOPERATOR_H_
#define PROJECTIONOPERATOR_H_

#include "Operator.h"
#include <vector>
class ProjectionOperator: public Operator {
private:
	Operator* input;
	const std::vector<int>& attributes;

public:
	ProjectionOperator(Operator* input, const std::vector<int>& attributes);
	virtual ~ProjectionOperator();
	void open();
	// Produce the next tuple by taking attributes specified as in vector
	bool next();
	// Get all produced values
	std::vector<Register*> getOutput();
	// Close the operator
	void close();
};

#endif /* PROJECTIONOPERATOR_H_ */
