/*
 * TableScanOperator.h
 *
 *  Created on: Jun 11, 2012
 *      Author: alexander
 */

#ifndef TABLESCANOPERATOR_H_
#define TABLESCANOPERATOR_H_

#include "Operator.h"
#include "../schema/Schema.h"
#include "../buffer/BufferManager.h"
#include "../segments/Structs.h"
#include "../segments/SPSegment.h"
#include "../segments/RelationSegment.h"
#include <memory>

/**
 * Performs a table scan on a relation,
 * i.e. reads every tuple of a RelationSegment
 * and writes its content to the registers
 */
class TableScanOperator: public Operator {
private:
	const Schema::Relation* relation;
	const SPSegment* segment;
	unique_ptr<Schema::Relation> relationUniquePtr; // iff ts-operator
	unique_ptr<RelationSegment> segmentUniquePtr;

	BufferManager* bufferManager;
	unsigned currentPage = 0;         // page id of currently looked page
	unsigned segmentPageIndex = 0;    // logical id while iterating through segment
	unsigned tupleOnPage = 0;         // tuple looked at on currentPage

	// copys data to register (pointing to buffer frame in main memory)
	void copyToRegister(char* data, unsigned length);

public:
	/**
	 * Looks up the relation segment in meta data
	 * using pointers since relation can be looked up
	 * by other constructors as well
	 */
	TableScanOperator(const Schema::Relation* relation, BufferManager* bufferManager);

	/**
	 * Looks up Schema::Relation tuple first in meta data
	 */
	TableScanOperator(const std::string& name, BufferManager* bufferManager);

	/**
	 * Looks up the relation segment in meta data
	 * using pointers since relation can be looked up
	 * by other constructors as well
	 */
	TableScanOperator(unique_ptr<Schema::Relation> relation, unique_ptr<RelationSegment> segment, BufferManager* bufferManager);

	TableScanOperator(const Schema::Relation* relation, const SPSegment* segment, BufferManager* bufferManager);

	virtual ~TableScanOperator();

	void open();

	// Produce the next tuple
	bool next();

	// Get all produced values
	std::vector<Register*> getOutput();

	// Close the operator
	void close();
};

#endif /* TABLESCANOPERATOR_H_ */
