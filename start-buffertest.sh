#!/bin/sh
# first make it
make
# generate test database file 
tools/datafile.sh bin/test-db.dat 50

bin/buffertest bin/test-db.dat 50 10 4

# delete it again
# rm -f bin/test-db.dat
