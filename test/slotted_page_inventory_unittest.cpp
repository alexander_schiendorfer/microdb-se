#include <limits.h>
#include "gtest/gtest.h"
#include <iostream>
#include "unittest_tools.h"
#include "../util/Util.h"
#include "../util/tid.h"
#include "../buffer/BufferManager.h"
#include "../buffer/BufferFrameHolder.h"
#include "../buffer/BufferFrame.h"
#include "../segments/SegmentInventory.h"
#include <memory>
#include <string>
#include <set>
#include <stdexcept>
#include <cstring>

using namespace std;

/** SlottedPageInventroy unit test */

// The fixture for testing class BufferManagerTest.
class SlottedPageInventoryTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.
  BufferManager bm;
  SegmentInventory si;

  int dbFileDescr;
  const static int pageSize = 4096;
  char buffer[pageSize]; // exactly one page here

  SlottedPageInventoryTest() : bm(UNIT_TEST_FILE, PAGES_IN_BUFFER_LARGER), si(bm) {
    // You can do set-up work for each test here.
  }

  virtual ~SlottedPageInventoryTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  // Objects declared here can be used by all tests in the test case for Foo.
  char* getHelpString(char firstChar, unsigned paddingLength) {
	  char* newStr = new char[paddingLength];
	  newStr[0] = firstChar;
	  for(unsigned i = 1; i < paddingLength; i++) {
		  newStr[i] = 'a' + (i-1) % 26;
	  }
	  return newStr;
  }

};

// Tests basic conversion to TID
TEST_F(SlottedPageInventoryTest, Basic) {
  // This test is named "Negative", and belongs to the "FactorialTest"
  // test case.
  TID newTid = Util::getTID(4711, 3);
	// This test is named "Negative", and belongs to the "FactorialTest"
  // test case.
  unsigned pageId = newTid.pageId;
  unsigned short slotId = newTid.slotId;

  ASSERT_EQ((unsigned short) 2, sizeof(slotId));
  ASSERT_EQ((unsigned ) 4, sizeof(pageId));
  ASSERT_EQ((unsigned) 4711, pageId);
  ASSERT_EQ((unsigned short) 3, slotId);
}

TEST_F(SlottedPageInventoryTest, createSegmentTest) {
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();

	ASSERT_EQ((unsigned) 5, spSegPtr->getSize());
	ASSERT_EQ(regularSeg->getID(), spSegPtr->getID());
	si.dropSegment(spSegPtr->getID());
}

TEST_F(SlottedPageInventoryTest, insertRecordTest) {
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();
	std::string testStr("Hallo");

	Record r(5, testStr.c_str());
	TID tid = spSegPtr->insert(r);

	// when using lookup - we will get a new record object
	unique_ptr<Record> record(spSegPtr->lookup(tid));

	Record* rec = record.get();
	std::string actual(static_cast<const char*>(rec->getData()), rec->getLen());
	ASSERT_TRUE(testStr.compare(actual) == 0);
	si.dropSegment(spSegPtr->getID());
}

TEST_F(SlottedPageInventoryTest, insertHugeRecordTest) {
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();

    // allocate very large tuple (PAGESIZE)
	unique_ptr<char> testStr(new char[PAGESIZE]);

	Record r(PAGESIZE, testStr.get());

	// this needs to throw a runtime error
	ASSERT_THROW(spSegPtr->insert(r), runtime_error);
	si.dropSegment(regularSeg->getID());
}

TEST_F(SlottedPageInventoryTest, removeRecordTest) {
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();
	std::string testStr("Hallo");

	Record r(5, testStr.c_str());
	TID tid = spSegPtr->insert(r);
	unsigned prevFreeSpace;

	// state after insert
	// get page here
	{
		BufferFrameHolder holder(bm, tid.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(headerOfPage.slotCount, (unsigned) 1);
		ASSERT_EQ(headerOfPage.unusedSlots, (unsigned) 0);
		prevFreeSpace = headerOfPage.freeSpace;

		// check offset pointer ( should be len from the end)
		Slot& s = headerOfPage.firstSlot[tid.slotId];
		ASSERT_EQ(PAGESIZE-r.getLen(), s.offset);

		// calculate ptr to tuple
		char* tuplePtr = static_cast<char*>(frame.getData()) + s.offset;
		ASSERT_EQ(tuplePtr, static_cast<char*>(headerOfPage.dataStart));
		holder.setDirty(false);
	}
	bool del = spSegPtr->remove(tid);

	ASSERT_TRUE(del);
	// state after insert
	// get page here
	{
		BufferFrameHolder holder(bm, tid.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(headerOfPage.slotCount, (unsigned)1);
		ASSERT_EQ(headerOfPage.unusedSlots,(unsigned) 1);
		ASSERT_EQ(r.getLen(), headerOfPage.freeSpace-prevFreeSpace);

		// calculate ptr to tuple
		// should point to start again (saves unnecessary holes)s
		char* tuplePtr = static_cast<char*>(frame.getData()) + PAGESIZE;
		ASSERT_EQ(tuplePtr, static_cast<char*>(headerOfPage.dataStart));
		holder.setDirty(false);
	}
	del = spSegPtr->remove(tid);
	ASSERT_FALSE(del);
	// when using lookup - we will get an exception
	ASSERT_THROW(spSegPtr->lookup(tid), runtime_error);
	si.dropSegment(spSegPtr->getID());
}

TEST_F(SlottedPageInventoryTest, InsertMultipleTuples) {
	// test setup: fill 1 page with 3 tuples, then assert that all three
	// tuples are on the same page (TID comp as well as actual binary layout)
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();

	// calculate junks for all that
	unsigned availableSize = PAGESIZE - sizeof(SPHeader) - 3 * sizeof(Slot);
	unsigned firstTupleSize = availableSize / 3 + availableSize % 3;
	unsigned otherTupleSize = availableSize / 3;

	// allocate string for first tuple - pattern of abc... stored
	char* firstStr = getHelpString('1', firstTupleSize);
	char* secondStr = getHelpString('2', otherTupleSize);
	char* thirdStr = getHelpString('3', otherTupleSize);
	char* fourthStr = getHelpString('4', otherTupleSize);

	Record firstRec(firstTupleSize, firstStr);
	Record secondRec(otherTupleSize, secondStr);
	Record thirdRec(otherTupleSize, thirdStr);

	// insert all three of them
	TID firstTID = spSegPtr->insert(firstRec);
	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(headerOfPage.slotCount, (unsigned)1);
		ASSERT_EQ(headerOfPage.unusedSlots, (unsigned)0);
		ASSERT_EQ(reinterpret_cast<char*>(headerOfPage.firstSlot), static_cast<char*>(frame.getData()) + sizeof(SPHeader));
		ASSERT_EQ(static_cast<char*>(headerOfPage.dataStart), static_cast<char*>(frame.getData())+PAGESIZE-firstTupleSize);
		holder.setDirty(false);
	}

	TID secTID = spSegPtr->insert(secondRec);
	{
		BufferFrameHolder holder(bm, secTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(headerOfPage.slotCount, (unsigned)2);
		ASSERT_EQ(headerOfPage.unusedSlots,(unsigned) 0);
		ASSERT_EQ(reinterpret_cast<char*>(headerOfPage.firstSlot), static_cast<char*>(frame.getData()) + sizeof(SPHeader));
		ASSERT_EQ(static_cast<char*>(headerOfPage.dataStart), static_cast<char*>(frame.getData())+PAGESIZE-firstTupleSize-otherTupleSize);
		holder.setDirty(false);
	}

	TID thirdTID = spSegPtr->insert(thirdRec);

	// they have to be on the same page now! TODO this might fall with a FSI impl
	// as it highly depends on the page choice strategy
	ASSERT_EQ(firstTID.pageId, secTID.pageId);
	ASSERT_EQ(thirdTID.pageId, secTID.pageId);


	// check header

	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(headerOfPage.slotCount,(unsigned) 3);
		ASSERT_EQ(headerOfPage.unusedSlots,(unsigned) 0);
		ASSERT_EQ(reinterpret_cast<char*>(headerOfPage.firstSlot), static_cast<char*>(frame.getData()) + sizeof(SPHeader));
		holder.setDirty(false);
	}
	// make sure we also get them correctly back?
	Record* firstRecPtr = spSegPtr->lookup(firstTID);
	ASSERT_EQ('1', ((char*)firstRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(firstRecPtr->getData(), firstStr, firstTupleSize) == 0);

	Record* secRecPtr = spSegPtr->lookup(secTID);
	ASSERT_EQ('2', ((char*)secRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(secRecPtr->getData(), secondStr, otherTupleSize) == 0);

	Record* thirdRecPtr = spSegPtr->lookup(thirdTID);
	ASSERT_EQ('3', ((char*)thirdRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(thirdRecPtr->getData(), thirdStr, otherTupleSize) == 0);

	// a fourth cannot be there!
	Record fourthRec(otherTupleSize, fourthStr);
	TID fourthTID = spSegPtr->insert(fourthRec);
	ASSERT_NE(firstTID.pageId, fourthTID.pageId);

	si.dropSegment(spSegPtr->getID());

	delete[] firstStr; delete[] secondStr; delete[] thirdStr; delete[] fourthStr;
}

TEST_F(SlottedPageInventoryTest, CompactifyTest) {
	// test setup: fill 1 page with 3 tuples, then assert that all three
	// tuples are on the same page (TID comp as well as actual binary layout)
	// then delete one and reinsert it
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();

	// calculate junks for all that
	unsigned availableSize = PAGESIZE - sizeof(SPHeader) - 3 * sizeof(Slot);
	unsigned firstTupleSize = availableSize / 3 + availableSize % 3;
	unsigned otherTupleSize = availableSize / 3;

	// allocate string for first tuple - pattern of abc... stored
	char* firstStr = getHelpString('1', firstTupleSize);
	char* secondStr = getHelpString('2', otherTupleSize);
	char* thirdStr = getHelpString('3', otherTupleSize);
	char* fourthStr = getHelpString('4', otherTupleSize - sizeof(Slot));

	Record firstRec(firstTupleSize, firstStr);
	Record secondRec(otherTupleSize, secondStr);
	Record thirdRec(otherTupleSize, thirdStr);
	Record fourthRec(otherTupleSize - sizeof(Slot), fourthStr);

	// insert all three of them
	TID firstTID = spSegPtr->insert(firstRec);
	TID secondTID = spSegPtr->insert(secondRec);
	TID thirdTID = spSegPtr->insert(thirdRec);

	// and do we get the third tuple as well?
	Record* thirdRecPtr = spSegPtr->lookup(thirdTID);
	ASSERT_EQ('3', ((char*)thirdRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(thirdRecPtr->getData(), thirdStr, otherTupleSize) == 0);


	unsigned oldThirdOffset, oldFirstOffset;
	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		const Slot* oldSlot = &headerOfPage.firstSlot[thirdTID.slotId];
		oldThirdOffset = oldSlot->offset;
		oldSlot = &headerOfPage.firstSlot[firstTID.slotId];
		oldFirstOffset = oldSlot->offset;
		holder.setDirty(false);
	}

	// creating hole
	spSegPtr->remove(secondTID);
	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		const Slot* oldSlot = &headerOfPage.firstSlot[thirdTID.slotId];

		ASSERT_EQ(oldThirdOffset, oldSlot->offset);
		holder.setDirty(false);
	}
	// make sure we also get them correctly back?
	Record* firstRecPtr = spSegPtr->lookup(firstTID);
	ASSERT_EQ('1', ((char*)firstRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(firstRecPtr->getData(), firstStr, firstTupleSize) == 0);

	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		Slot* oldSlot = &headerOfPage.firstSlot[thirdTID.slotId];
		ASSERT_EQ(oldThirdOffset, oldSlot->offset);

		oldSlot = &headerOfPage.firstSlot[firstTID.slotId];
		ASSERT_EQ(oldFirstOffset, oldSlot->offset);
		holder.setDirty(false);
	}
	// and do we get the third tuple as well?
	thirdRecPtr = spSegPtr->lookup(thirdTID);
	ASSERT_EQ('3', ((char*)thirdRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(thirdRecPtr->getData(), thirdStr, otherTupleSize) == 0);

	// reinsert has to find space on the same page now
	TID secondTID_new = spSegPtr->insert(fourthRec);

	// same page must have been taken
	ASSERT_EQ(secondTID.pageId, secondTID_new.pageId);
	// same slot must have been taken
	ASSERT_EQ(secondTID.slotId, secondTID_new.slotId);

	// offset of third tuple must have changed and first must remain unchanged
	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		BufferFrame& frame = holder.getProtectedFrame();
		SPHeader& headerOfPage = *static_cast<SPHeader*>(frame.getData());
		Slot* slot = &headerOfPage.firstSlot[thirdTID.slotId];
		ASSERT_NE(slot->offset, oldThirdOffset);

		slot = &headerOfPage.firstSlot[firstTID.slotId];
		ASSERT_EQ(oldFirstOffset, slot->offset);
		holder.setDirty(false);
	}

	// make sure we also get them correctly back?
	firstRecPtr = spSegPtr->lookup(firstTID);
	ASSERT_EQ('1', ((char*)firstRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(firstRecPtr->getData(), firstStr, firstTupleSize) == 0);

	Record* secRecPtr = spSegPtr->lookup(secondTID_new);
	ASSERT_EQ('4', ((char*)secRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(secRecPtr->getData(), fourthStr, otherTupleSize-sizeof(Slot)) == 0);

	thirdRecPtr = spSegPtr->lookup(thirdTID);
	ASSERT_EQ('3', ((char*)thirdRecPtr->getData())[0]);
	ASSERT_TRUE(strncmp(thirdRecPtr->getData(), thirdStr, otherTupleSize) == 0);

	delete[] firstStr; delete[] secondStr; delete[] thirdStr; delete[] fourthStr;
	si.dropSegment(spSegPtr->getID());
}

TEST_F(SlottedPageInventoryTest, UpdateTest) {
	// test setup ... 3 records will be inserted
	// 1. case smaller tuple update -> freespace, slot.length should be updated accordingly
	// 2. larger tuple than orginal but still fits on page -> should remain on page
	// 3. too large for page -> indirection
	// 4. update of indirected tuple
	unique_ptr<RegularSegment> regularSeg = si.createSegment(5,5);
	unique_ptr<SPSegment> spSeg = si.retrieveSPSegment(regularSeg->getID());
	SPSegment* spSegPtr = spSeg.get();

	// calculate junks for all that
	unsigned availableSize = PAGESIZE - sizeof(SPHeader) - 3 * sizeof(Slot);
	unsigned firstTupleSize = availableSize / 3 + availableSize % 3;
	unsigned otherTupleSize = availableSize / 3;

	// allocate string for first tuple - pattern of abc... stored
	char* firstStr = getHelpString('1', firstTupleSize);
	char* secondStr = getHelpString('2', otherTupleSize);
	char* thirdStr = getHelpString('3', otherTupleSize / 2);
	char* fourthStr = getHelpString('4', otherTupleSize * 2);

	Record firstRec(firstTupleSize, firstStr);
	Record secondRec(otherTupleSize, secondStr);
	Record thirdRec(otherTupleSize / 2, thirdStr);
	Record fourthRec(otherTupleSize * 2, fourthStr);

	// insert all three of them
	TID firstTID = spSegPtr->insert(firstRec);
	TID secondTID = spSegPtr->insert(secondRec);
	TID thirdTID = spSegPtr->insert(thirdRec);

	// should all fit on one page
	ASSERT_EQ(firstTID.pageId, secondTID.pageId);
	ASSERT_EQ(thirdTID.pageId, secondTID.pageId);

	// retrieve correct first entry
	Record* rec1 = spSegPtr->lookup(firstTID);
	ASSERT_EQ(firstTupleSize,rec1->getLen());
	ASSERT_TRUE(strncmp(rec1->getData(), firstStr, firstTupleSize) == 0);

	unsigned spaceBefore;
	// now update with smaller record
	char* smallerFirst = getHelpString('a', firstTupleSize / 2);

	{
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		spaceBefore = header.freeSpace;
		holder.setDirty(false);
	}

	Record firstRecSmaller(firstTupleSize/2, smallerFirst);
	spSegPtr->update(firstTID, firstRecSmaller);

	{ // check metadata
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		ASSERT_EQ(spaceBefore + (firstRec.getLen()-firstRecSmaller.getLen()), header.freeSpace);
		holder.setDirty(false);
	}

	rec1 = spSegPtr->lookup(firstTID);
	ASSERT_EQ(firstTupleSize/2,rec1->getLen());
	ASSERT_TRUE(strncmp(rec1->getData(), smallerFirst, firstTupleSize / 2) == 0);

	// now re-update this one (tuple is now larger but still fits on page!)
	spSegPtr->update(firstTID, firstRec);
	rec1 = spSegPtr->lookup(firstTID);
	ASSERT_EQ(firstTupleSize,rec1->getLen());
	ASSERT_TRUE(strncmp(rec1->getData(), firstStr, firstTupleSize) == 0);

	{ // check metadata - we expect to have found enough space on same page
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		const Slot* firstRecSlot = &header.firstSlot[firstTID.slotId];
		ASSERT_EQ(0, firstRecSlot->isReference);
		spaceBefore = header.freeSpace;
		holder.setDirty(false);
	}

	// and now we double that entry such that it won't fit on the same page anymore
	spSegPtr->update(firstTID, fourthRec);
	rec1 = spSegPtr->lookup(firstTID);
	ASSERT_EQ(firstTupleSize * 2,rec1->getLen());
	ASSERT_TRUE(strncmp(rec1->getData(), fourthStr, 2*firstTupleSize) == 0);

	TID internalTID;
	{ // check metadata - we expect not to have found enough space on same page
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		const Slot* firstRecSlot = &header.firstSlot[firstTID.slotId];
		ASSERT_EQ(1, firstRecSlot->isReference);
		// memory had to be freed correctly
		ASSERT_EQ(spaceBefore + otherTupleSize, header.freeSpace);

		const ReferenceSlot* refSlot = reinterpret_cast<const ReferenceSlot*>(firstRecSlot);
		internalTID = refSlot->tid;
		holder.setDirty(false);
	}

	unsigned refSlots;
	{ // check metadata of referenced page
		BufferFrameHolder holder(bm, internalTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		const Slot* firstRecSlot = &header.firstSlot[internalTID.slotId];
		ASSERT_EQ(0, firstRecSlot->isReference);
		ASSERT_EQ(1, firstRecSlot->wasMoved);
		ASSERT_EQ(fourthRec.getLen() + sizeof(TID), firstRecSlot->length);
		const TID* tidPtr = reinterpret_cast<const TID*>(static_cast<char*>(frame.getData()) + firstRecSlot->offset);
		ASSERT_EQ(firstTID.pageId, tidPtr->pageId);
		ASSERT_EQ(firstTID.slotId, tidPtr->slotId);
		spaceBefore = header.freeSpace;
		refSlots = header.unusedSlots;
		holder.setDirty(false);
	}

	// if I reinsert a smaller tuple, we should get rid of this indirection
	spSegPtr->update(firstTID, firstRec);
	rec1 = spSegPtr->lookup(firstTID);
	ASSERT_EQ(firstTupleSize,rec1->getLen());
	ASSERT_TRUE(strncmp(rec1->getData(), firstStr, firstTupleSize) == 0);

	{ // check metadata - we expect to have found enough space on same page
		BufferFrameHolder holder(bm, firstTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		const Slot* firstRecSlot = &header.firstSlot[firstTID.slotId];
		ASSERT_EQ(0, firstRecSlot->isReference);
		holder.setDirty(false);
	}

	{ // check metadata of referenced page
		BufferFrameHolder holder(bm, internalTID.pageId, true);
		const BufferFrame& frame = holder.getProtectedFrame();
		const SPHeader& header = *static_cast<SPHeader*>(frame.getData());
		const Slot* firstRecSlot = &header.firstSlot[internalTID.slotId];
		ASSERT_EQ(0, firstRecSlot->isReference);
		ASSERT_EQ(1, firstRecSlot->unused);
		ASSERT_EQ(refSlots + 1, header.unusedSlots);
		ASSERT_EQ(spaceBefore + 2 * otherTupleSize + sizeof(TID), header.freeSpace);

		holder.setDirty(false);
	}


	delete[] firstStr; delete[] secondStr; delete[] thirdStr; delete[] smallerFirst; delete[] fourthStr;
	si.dropSegment(spSegPtr->getID());
}

TEST_F(SlottedPageInventoryTest, FreeSpaceInventoryTest)  {
	std::list<ExtentListEntry> entryList;

	// first extent
	ExtentListEntry e1;
	e1.length = 6;
	e1.segmentID = 1;
	e1.startPage = 2;

	// first extent
	ExtentListEntry e2;
	e2.length = 3;
	e2.segmentID = 1;
	e2.startPage = 13;

	// first extent
	ExtentListEntry e3;
	e3.length = 4;
	e3.segmentID = 1;
	e3.startPage = 27;

	entryList.push_back(e1);
	entryList.push_back(e2);
	entryList.push_back(e3);

	// now find correct pages
	SPSegment segment(bm, 1, entryList, si, true);
	for(unsigned i = 0; i < (unsigned) 6; i++) {
		ASSERT_EQ(2 + i, segment[i]);
		segment[i];
	}

	for(unsigned i = 6; i < (unsigned) 9; i++) {
		ASSERT_EQ(7 + i, segment[i]);
		segment[i];
	}

	for(unsigned i = 9; i < (unsigned) 13; i++) {
		ASSERT_EQ(18 + i, segment[i]);
		segment[i];
	}
}

int main(int argc, char **argv) {
  // create test file and runs it
  createTestfile(UNIT_TEST_FILE, PAGES_OF_LARGER_FILE);
  BufferManager* bm = new BufferManager(UNIT_TEST_FILE, PAGES_IN_BUFFER_LARGER);
  SegmentInventory* si = new SegmentInventory(*bm);
  si->install();
  delete si;
  delete bm;

  ::testing::InitGoogleTest(&argc, argv);
  int tests = RUN_ALL_TESTS();
  //deleteTestfile(UNIT_TEST_FILE);
  return tests;
}

