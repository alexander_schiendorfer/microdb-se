/*
 * unittest_tools.h
 *
 *  Created on: May 13, 2012
 *      Author: alexander
 */

#ifndef UNITTEST_TOOLS_H_
#define UNITTEST_TOOLS_H_
#include <string>

const std::string UNIT_TEST_FILE = "bin/testdb.dat";
const std::string PAGES_OF_FILE ="20";
const int PAGES_IN_BUFFER = 4;

const int PAGES_IN_BUFFER_LARGER = 20;
const std::string PAGES_OF_LARGER_FILE = "50";


void createTestfile(const std::string& file, const std::string& filePages){
	system(("tools/datafile.sh "+ file + " " + filePages).c_str());
}

void deleteTestfile(const std::string& file) {
	  system(("rm -f "+file).c_str());
}

#endif /* UNITTEST_TOOLS_H_ */
