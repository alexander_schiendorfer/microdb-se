#include <limits.h>
#include "../buffer/BufferManager.h"
#include "../buffer/BufferFrame.h"
#include "../operators/Operator.h"
#include "../operators/PrintOperator.h"
#include "../operators/TableScanOperator.h"
#include "../operators/Register.h"
#include "../operators/ProjectionOperator.h"
#include "../operators/SelectionOperator.h"
#include "../operators/SortOperator.h"
#include "../operators/MergeOperator.h"
#include "../schema/Schema.h"
#include "../schema/Types.h"
#include "../segments/SegmentInventory.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <string>
#include <stdexcept>
#include <pthread.h>
#include <iostream>
#include <unistd.h>   // for open/close ...
#include <fcntl.h>
#include <pthread.h>
#include <vector>
#include <memory>
#include "unittest_tools.h"

namespace {

class MockingOperator : public Operator {
	struct privRec {
		std::string first;
		int second;
		std::string third;
	};

	privRec* records;
	int curRec;

public:
	MockingOperator() {
		records = new privRec[3];

		output.resize(3, NULL);
		for(int i = 0; i < 3; i++)
			output[i] = new Register();

		output[0]->setInt(false);
		output[1]->setInt(true);
		output[2]->setInt(false);

		records[0].first = "abc";
		records[0].second = 123;
		records[0].third = "def";

		records[1].first = "ghi";
		records[1].second = 456;
		records[1].third = "jkl";

		records[2].first = "mno";
		records[2].second = 789;
		records[2].third = "pqr";
	}

	virtual ~MockingOperator() {
		for(int i = 0; i < 3; i++) {
			delete output[i];
		}
		delete[] records;
	}

	void open() {
		curRec = 0;

	}
	// Produce the next tuple
	bool next() {
		if(curRec >= 3)
			return false;
		else {
			output[0]->setString(records[curRec].first);
			output[1]->setInteger(records[curRec].second);
			output[2]->setString(records[curRec].third);

			curRec++;
			return true;
		}
	}
	// Get all produced values
	std::vector<Register*> getOutput() {
		return output;
	}

	// Close the operator
	void close() {

	}
};

// The fixture for testing class BufferManagerTest.
class OperatorTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.
  BufferManager bm;
  SegmentInventory si;
  int dbFileDescr;
  const static int pageSize = 4096;
  char buffer[pageSize]; // exactly one page here
  RelationSegment* relSegPtr;
  unique_ptr<RelationSegment> relSeg;
  Schema::Relation testRelation;
  Schema::Relation mergeRelation1;
  RelationSegment* relSegPtr1;
  unique_ptr<RelationSegment> relSeg1;
  Schema::Relation mergeRelation2;
  RelationSegment* relSegPtr2;
  unique_ptr<RelationSegment> relSeg2;

  OperatorTest() : bm(UNIT_TEST_FILE, PAGES_IN_BUFFER_LARGER), si(bm), testRelation("tests"), mergeRelation1("Schurken"), mergeRelation2("Helden") {
    // You can do set-up work for each test here.
  }

  virtual ~OperatorTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
	    createSingleTestRelation();
	    //testWorking();
	  	createMergeRelations();
	  	//testWorking();
  }

  void createSingleTestRelation() {
	    // Code here will be called immediately after the constructor (right
	    // before each test).
		Schema::Relation::Attribute first, second, third;

		testRelation.attributes.clear();
		// fill attributes
		unsigned strLength = 6;
		first.type = Types::Tag::Integer;
		second.type = Types::Tag::Char; second.len = strLength;
		third.type = Types::Tag::Integer;

		testRelation.attributes.push_back(first);
		testRelation.attributes.push_back(second);
		testRelation.attributes.push_back(third);

		// insert two tuples in the according relation
		unique_ptr<RegularSegment> seg = si.createSegment(2, 2);
		RegularSegment* segPtr = seg.get(); // only because of eclipse -.-
		unsigned segId = segPtr->getID();
		relSeg = si.retrieveRelationSegment(segId, &testRelation);
		relSegPtr = relSeg.get();

		// manual record creation

		unsigned bufferLen = 2*sizeof(int) + strLength;
		char buffer[bufferLen];
		char* strings[3];

		strings[0] = "hello1";
		strings[1] = "makeIt";
		strings[2] = "makeIt";

		vector<unique_ptr<Register> > tuple;
		unique_ptr<Register> ptr1(new Register()),ptr2(new Register()),ptr3(new Register());
		tuple.push_back(std::move(ptr1));
		tuple.push_back(std::move(ptr2));
		tuple.push_back(std::move(ptr3));

		tuple[0]->setInt(true);
		tuple[1]->setInt(false);
		tuple[2]->setInt(true);

		for(int i = 0; i < 3; i++) {

			tuple[0]->setInteger(i+1);
			tuple[1]->setString(strings[i]);
			tuple[2]->setInteger(2*(i+1)-1);

			relSegPtr->insertTuple(tuple);
		}
  }

  void createMergeRelations() {
	  	Schema::Relation::Attribute first1, second1, third1;
	  	Schema::Relation::Attribute first2, second2;

	  	mergeRelation1.attributes.clear();
	  	mergeRelation2.attributes.clear();

	  	// fill attributes
	  	unsigned strLength = 20;
	  	first1.type = Types::Tag::Integer;
	  	second1.type = Types::Tag::Char; second1.len = strLength;
	  	third1.type = Types::Tag::Integer;

	  	first2.type = Types::Tag::Integer;
	  	second2.type = Types::Tag::Char; second2.len = strLength;

	  	mergeRelation1.attributes.push_back(first1);
	  	mergeRelation1.attributes.push_back(second1);
	  	mergeRelation1.attributes.push_back(third1);

	  	mergeRelation2.attributes.push_back(first2);
	  	mergeRelation2.attributes.push_back(second2);

	  	// insert two tuples in the according relation
	  	unique_ptr<RegularSegment> seg1 = si.createSegment(2, 2);
	  	unique_ptr<RegularSegment> seg2 = si.createSegment(2, 2);
	  	RegularSegment* segPtr1 = seg1.get(); // only because of eclipse -.-
	  	RegularSegment* segPtr2 = seg2.get(); // only because of eclipse -.-
	  	unsigned segId1 = segPtr1->getID();
	  	unsigned segId2 = segPtr2->getID();
	  	relSeg1 = si.retrieveRelationSegment(segId1, &mergeRelation1);
	  	relSegPtr1 = relSeg1.get();

	  	relSeg2 = si.retrieveRelationSegment(segId2, &mergeRelation2);
	  	relSegPtr2 = relSeg2.get();

	  	char* strings[8];

	  	strings[0] = "Joker";
	  	strings[1] = "Two-Face";
	  	strings[2] = "Mr. Frost";
	  	strings[3] = "Lex Luther";
	  	strings[4] = "Tom";
	  	strings[5] = "Bill Gates";
	  	strings[6] = "Funky";
	  	strings[7] = "Mozart";

	  	int ints[8];
	  	ints[0] = 1;
	  	ints[1] = 1;
	  	ints[2] = 1;
	  	ints[3] = 2;
	  	ints[4] = 3;
	  	ints[5] = 4;
	  	ints[6] = 5;
	  	ints[7] = 42;

	  	vector<unique_ptr<Register> > tuple1;
	  	unique_ptr<Register> ptr1(new Register()),ptr2(new Register()),ptr3(new Register());
	  	tuple1.push_back(std::move(ptr1));
	    tuple1.push_back(std::move(ptr2));
	    tuple1.push_back(std::move(ptr3));

	  	tuple1[0]->setInt(true);
	  	tuple1[1]->setInt(false);
	  	tuple1[2]->setInt(true);

	  	for(int i = 0; i < 8; i++) {

		  	tuple1[0]->setInteger(i+1);
		  	tuple1[1]->setString(strings[i]);
		  	tuple1[2]->setInteger(ints[i]);

	  		relSegPtr1->insertTuple(tuple1);
	  	}


	  	vector<unique_ptr<Register> > tuple2;
	  	unique_ptr<Register> ptr12(new Register()),ptr22(new Register());
	  	tuple2.push_back(std::move(ptr12));
	    tuple2.push_back(std::move(ptr22));

	  	tuple2[0]->setInt(true);
	  	tuple2[1]->setInt(false);

	  	char* strings2[5];
	  	strings2[0] = "Batman";
	  	strings2[1] = "Superman";
	  	strings2[2] = "Jerry";
	  	strings2[3] = "Steve Jobs";
	  	strings2[4] = "SE 2011";

	  	for(int i = 0; i < 5; i++) {

		  	tuple2[0]->setInteger(i+1);
		  	tuple2[1]->setString(strings2[i]);

	  		relSegPtr2->insertTuple(tuple2);
	  	}
  }

  void testWorking() {
		TableScanOperator to(&testRelation, relSegPtr, &bm);
		PrintOperator po(&to, std::cout);
		po.open();
		int tuples = 0;

		while(po.next()) {
			tuples++;
		}
		po.close();
		ASSERT_EQ(3, tuples);
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
	  si.dropSegment(relSegPtr->getID());
	  si.dropSegment(relSegPtr1->getID());
	  si.dropSegment(relSegPtr2->getID());
  }

  // Objects declared here can be used by all tests in the test case for Foo.

};

/**
 * Uses a mock input to get the print operator to work
 */
TEST_F(OperatorTest, PrintOperatorTest) {
	MockingOperator mock;
	PrintOperator po(&mock, std::cout);
	po.open();
	int tuples = 0;

	while(po.next()) {
		tuples++;
	}

	po.close();
	ASSERT_EQ(3, tuples);
}

TEST_F(OperatorTest, MergeOperatorTest_Boesewichte) {
	TableScanOperator tso1(&mergeRelation1, relSegPtr1, &bm);
	TableScanOperator tso2(&mergeRelation2, relSegPtr2, &bm);
	pair<unsigned int, unsigned int> att (2,0);
	vector<pair<unsigned int, unsigned int>> atts;
	atts.push_back(att);
	MergeOperator mo(&tso1, &tso2, atts);

	PrintOperator po1(&mo, cout);

	po1.open();
	while(po1.next());
	po1.close();
}


TEST_F(OperatorTest, MergeOperatorTest) {
	MockingOperator mock1;
	MockingOperator mock2;
	pair<unsigned int, unsigned int> att (1,1);
	vector<pair<unsigned int, unsigned int>> atts;
	atts.push_back(att);
	MergeOperator mo(&mock1, &mock2, atts);
	PrintOperator po(&mo, cout);
	po.open();

	while(po.next());

	po.close();
}

TEST_F(OperatorTest, SortOperatorTest) {
	TableScanOperator to(&testRelation, relSegPtr, &bm);
	std::vector<unsigned> sortAttr; sortAttr.push_back(0);
	SortOperator so(&to, false, sortAttr);
	PrintOperator po(&so, std::cout);
	po.open();
	int tuples = 0;

	while(po.next()) {
		tuples++;
	}
	po.close();
	ASSERT_EQ(3, tuples);
}

TEST_F(OperatorTest, SortOperatorTest_Boesewichte) {
	TableScanOperator to(&mergeRelation1, relSegPtr1, &bm);
	std::vector<unsigned> sortAttr; sortAttr.push_back(1);
	SortOperator so(&to, true, sortAttr);
	PrintOperator po(&so, std::cout);
	po.open();
	int tuples = 0;

	while(po.next()) {
		tuples++;
	}
	po.close();
	ASSERT_EQ(8, tuples);
}

TEST_F(OperatorTest, TableScanOperatorTest) {
	TableScanOperator to(&testRelation, relSegPtr, &bm);
	PrintOperator po(&to, std::cout);
	po.open();
	int tuples = 0;

	while(po.next()) {
		tuples++;
	}
	po.close();
	ASSERT_EQ(3, tuples);
}

TEST_F(OperatorTest, SelectionOperatorTest) {
	TableScanOperator to(&testRelation, relSegPtr, &bm);
	std::vector<int> attributes; std::vector<Register*> constants;
	attributes.push_back(0); Register* constant = new Register();
	constant->setInt(true);
	constant->setInteger(3);
    constants.push_back(constant);
	SelectionOperator so(&to, attributes, constants);
	PrintOperator po(&so, std::cout);
	po.open();
	int tuples = 0;

	std::cout << "select * from t where 1 = 3 (expecting 1 tuple)" << std::endl;
	while(po.next()) {
		tuples++;
	}
	po.close();
	ASSERT_EQ(1, tuples);

	// now testing strings and multiple selected tuples (2-coverage :>)
	constant->setInt(false);
	constant->setString("makeIt");
	attributes[0] = 1;
	TableScanOperator t2(&testRelation, relSegPtr, &bm);
	SelectionOperator so2(&t2, attributes, constants);
	PrintOperator po2(&so2, std::cout);
	po2.open();
	tuples = 0;
	std::cout << "select * from t where 2 = 'makeIt' (expecting 2 tuples)" << std::endl;
	while(po2.next()) {
		tuples++;
	}
	po2.close();
	ASSERT_EQ(2, tuples);
	delete constant;
}

TEST_F(OperatorTest, ProjectionOperatorTest) {

	TableScanOperator to(&testRelation, relSegPtr, &bm);
	std::vector<int> attributes;
	attributes.push_back(0);
	attributes.push_back(2);
	ProjectionOperator pro(&to, attributes);
	PrintOperator po(&pro, std::cout);
	po.open();
	int tuples = 0;

	// attributes 0 and 2
	std::cout << "project on attributes 0 and 2 " << std::endl;
	while(po.next()) {
		tuples++;
	}
	po.close();
	ASSERT_EQ(3, tuples);


	// attributes 0 and 1
	std::cout << "project on attributes 0 and 1 " << std::endl;
	attributes.clear(); attributes.push_back(0); attributes.push_back(1);
	TableScanOperator ts2(&testRelation, relSegPtr, &bm);
	ProjectionOperator pro2(&ts2, attributes);
	PrintOperator po2(&pro2, std::cout);
	po2.open();
	tuples = 0;
	while(po2.next()) {
			tuples++;
	}
	po2.close();
	ASSERT_EQ(3, tuples);

	// attributes 1 and 2
	attributes.clear();	attributes.push_back(1); attributes.push_back(2);
	std::cout << "project on attributes 1 and 2 " << std::endl;
	TableScanOperator ts3(&testRelation, relSegPtr, &bm);
	ProjectionOperator pro3(&ts3, attributes);
	PrintOperator po3(&pro3, std::cout);
	po3.open();
	tuples = 0;
	while(po3.next()) {
			tuples++;
	}
	po3.close();
	ASSERT_EQ(3, tuples);

	// attributes 2 and 0
	attributes.clear(); attributes.push_back(2); attributes.push_back(0);

	std::cout << "project on attributes 2 and 0 " << std::endl;
	TableScanOperator ts4(&testRelation, relSegPtr, &bm);
	ProjectionOperator pro4(&ts4, attributes);
	PrintOperator po4(&pro4, std::cout);

	tuples = 0;
	po4.open();
	while(po4.next()) {
		tuples++;
	}
	po4.close();
	ASSERT_EQ(3, tuples);
}

}
int main(int argc, char **argv) {
  // create test file and runs it
  std::cout << "in this unit main function" << std::endl;
  createTestfile(UNIT_TEST_FILE, "100"); //TODO - review
  BufferManager* bm = new BufferManager(UNIT_TEST_FILE, 40);
  SegmentInventory* si = new SegmentInventory(*bm);
  si->install();

  delete si;
  delete bm;
  ::testing::InitGoogleTest(&argc, argv);
  int tests = RUN_ALL_TESTS();
  deleteTestfile(UNIT_TEST_FILE);
  return tests;
}

