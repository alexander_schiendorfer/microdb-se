#include <limits.h>
#include "../buffer/BufferManager.h"
#include "../buffer/BufferFrame.h"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <string>
#include <stdexcept>
#include <pthread.h>
#include <iostream>
#include <unistd.h>   // for open/close ...
#include <fcntl.h>
#include <pthread.h>

const string UNIT_TEST_FILE = "bin/testdb.dat";
const string PAGES_OF_FILE ="20";
const int PAGES_IN_BUFFER = 4;

namespace {

// The fixture for testing class BufferManagerTest.
class SegmentInventoryTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.
  BufferManager bm;
  int dbFileDescr;
  const static int pageSize = 4096;
  char buffer[pageSize]; // exactly one page here

  SegmentInventoryTest() {
    // You can do set-up work for each test here.
  }

  virtual ~SegmentInventoryTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  // Objects declared here can be used by all tests in the test case for Foo.

};

/**
 * Tests the basic read/write mechanisms by loading a page exclusively
 * , writing 1 2 3 4 to it, unfixing it
 * loading 4 other pages so it has to be written out
 * unfixing one of the other 5 pages
 * and reloading it and reading 1 2 3 4
 */
TEST_F(SegmentInventoryTest, Basic) {
	  // This test is named "Negative", and belongs to the "FactorialTest"
	  // test case.
	BufferFrame& bf = bm.fixPage(0, true);
	uint* castedPointer = reinterpret_cast<unsigned*>(bf.getData());
	for(unsigned i = 0; i < 4; i++) castedPointer[i] = i+1;
	bm.unfixPage(bf, true);

	BufferFrame& lastFrame = bm.fixPage(1, false);
	for(int i = 2; i <= PAGES_IN_BUFFER; i++) {
		lastFrame = bm.fixPage(i, false);
	}

	// page 0 must have been paged out by now
	bm.unfixPage(lastFrame, false );
	BufferFrame& bf2 = bm.fixPage(0, false);
	castedPointer = reinterpret_cast<unsigned*>(bf2.getData());
	for(unsigned i = 0; i < 4; i++) {
		ASSERT_EQ(i+1, castedPointer[i]);
	}
}

/**
 * Requires the buffer manager to throw an exception
 * if a wrong file name is specified
 */
TEST_F(SegmentInventoryTest, InvalidFileName) {
	ASSERT_THROW(new BufferManager("anyFileYouWant", 30), runtime_error);
	// false buffer manager is already deleted, a second statement is not useful
}

/**
* Allocates a buffer with 4 manageable frames;
* fixes 4 pages and tries to allocate a 5th page, hence
* waits for exception
*/
TEST_F(BufferManagerTest, Full) {
	for(int i = 0; i < PAGES_IN_BUFFER; i++) {
		bm.fixPage(i, false);
	}
	ASSERT_THROW(bm.fixPage(PAGES_IN_BUFFER, false), runtime_error);
}

TEST_F(BufferManagerTest, WriteWhenDelete) {
	string secondFile = UNIT_TEST_FILE + "2";
	system(("tools/datafile.sh "+ secondFile + " " + PAGES_OF_FILE).c_str());
	BufferManager* bm2 = new BufferManager(secondFile, PAGES_IN_BUFFER);

	BufferFrame& firstFrame = bm2->fixPage(0, true);
	BufferFrame& secondFrame = bm2->fixPage(1, false); // we won't get to rescue this guy :(

	uint* castedPointer = reinterpret_cast<unsigned*>(firstFrame.getData());
	uint* castedSecondPointer = reinterpret_cast<unsigned*>(secondFrame.getData());

	for(unsigned i = 0; i < 4; i++)  {
		castedPointer[i] = i+1;
		castedSecondPointer[i] = i + 1;
	}

	bm2->unfixPage(firstFrame, true);
	// no unfix for second frame

	// here I need to read the file
	dbFileDescr = open(secondFile.c_str(), O_RDONLY);

	// reading contents from first frame
	pread(dbFileDescr, buffer, pageSize, 0);
	castedPointer = reinterpret_cast<unsigned*>(buffer);
	for(unsigned i = 0; i < 4; i++) {
		ASSERT_EQ((unsigned)0, castedPointer[i]);
	}

	// reading second frame
	pread(dbFileDescr, buffer, pageSize, pageSize);
	castedPointer = reinterpret_cast<unsigned*>(buffer);
	for(unsigned i = 0; i < 4; i++) {
		ASSERT_EQ((unsigned)0, castedPointer[i]);
	}


	delete bm2; // now all should've been written, will need to check that

	pread(dbFileDescr, buffer, pageSize, 0);
	castedPointer = reinterpret_cast<unsigned*>(buffer);
	for(unsigned i = 0; i < 4; i++) {
		ASSERT_EQ(i+1, castedPointer[i]);
	}

	// our second frame should have remained unchanged
	pread(dbFileDescr, buffer, pageSize, pageSize);
	castedPointer = reinterpret_cast<unsigned*>(buffer);
	for(unsigned i = 0; i < 4; i++) {
		ASSERT_EQ((unsigned)0, castedPointer[i]);
	}

	system(("rm -f "+secondFile).c_str());
}

}  // namespace

/**
 * Performs a write operation on file followed
 * by an immediate read by two threads
 * they need to read the same content and then
 * unfix
 */
TEST_F(BufferManagerTest, ConcurrentRead) {
	pthread_attr_t pattr;
	pthread_attr_init(&pattr);
	pthread_t firstThread, secondThread;

	const unsigned magicNumber = 9876;
	// write magic number into page 3
	dbFileDescr = open(UNIT_TEST_FILE.c_str(), O_RDWR);
	pread(dbFileDescr, buffer, pageSize, 2 * pageSize);
	unsigned* helpPointer = reinterpret_cast<unsigned*>(buffer);
	*helpPointer = magicNumber;
	pwrite(dbFileDescr, buffer, pageSize, 2 * pageSize);

	// now read that number in two concurrent threads
	ReadParams rp1, rp2;
	rp1.bm = &bm;
	rp2.bm = &bm;
	ASSERT_NE(rp1.readNumber, magicNumber);
	ASSERT_NE(rp2.readNumber, magicNumber);


	pthread_create(&firstThread, &pattr, testRead, reinterpret_cast<void*>(&rp1));
	pthread_create(&secondThread, &pattr, testRead, reinterpret_cast<void*>(&rp2));

	pthread_join(firstThread, NULL);
	pthread_join(secondThread, NULL);

	ASSERT_EQ(magicNumber, rp1.readNumber);
	ASSERT_EQ(magicNumber, rp2.readNumber);
	// close used fd
	close(dbFileDescr);

	BufferFrame& frame1 = bm.fixPage(2, false); // should actually be a direct hit
	BufferFrame& frame2 = bm.fixPage(2, false); // since these are shared locks , no blocking

	ASSERT_EQ(&frame1, &frame2);
}

int main(int argc, char **argv) {
  // create test file and runs it
  std::cout << "in this main function" << std::endl;
  system(("tools/datafile.sh "+ UNIT_TEST_FILE + " " + PAGES_OF_FILE).c_str());
  ::testing::InitGoogleTest(&argc, argv);
  int tests = RUN_ALL_TESTS();
  system(("rm -f "+UNIT_TEST_FILE).c_str());
  return tests;
}

